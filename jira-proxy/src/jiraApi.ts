import * as request from 'request';

export interface JiraConfiguration {
    jiraUrl: string;
}

export interface JiraCredentialsData extends JiraConfiguration {
    username: string;
    password: string;
}
export interface JiraSessionData {
    session: {
        name: string;
        value: string;
    };
    loginInfo: {
        lastFailedLoginTime: string;
        previousLoginTime: string;
        loginCount: number;
        failedLoginCount: number;
    };
}
export async function getSession({
    jiraUrl,
    ...loginData
}: JiraCredentialsData): Promise<JiraSessionData> {
    return new Promise<JiraSessionData>((resolve, reject) =>
        request(
            {
                json: loginData,
                method: 'POST',
                strictSSL: false,
                url: `${jiraUrl}/rest/auth/1/session`
            },
            (error, response, body) => {
                if (error || !response || response.statusCode !== 200) {
                    reject({ code: 'UNAUTHORIZED', message: 'Jira authorization failure' });
                }
                resolve(body);
            }
        )
    );
}
