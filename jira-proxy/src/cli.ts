#!/usr/bin/env node
import { config } from 'dotenv';
import { dirname } from 'path';

import { startJiraTools } from '.';

config();

const JIRA_URL: string = process.env.JIRA_URL as string;
const PORT = parseInt(process.env.PORT || '80', 10);
const PUBLIC_RESOURCES_PATH = dirname(require.resolve('@mlewando/jira-tools-client'));

if (!JIRA_URL) {
    throw new Error('JIRA_URL not set in environment.');
}

console.log(`Running application with:
JIRA_URL: ${JIRA_URL}
client app is served from: ${PUBLIC_RESOURCES_PATH}
PORT: ${PORT}
`);

startJiraTools({
    jiraUrl: JIRA_URL,
    port: PORT,
    publicResources: PUBLIC_RESOURCES_PATH
});
