import { NextFunction, Request, Response } from 'express';
import { sign, verify } from 'jsonwebtoken';
import { isArray } from 'util';

export interface JwtResponse<T extends object> extends Response {
    locals: {
        payload: T;
        jwtToken: string;
    };
}

export function jiraToken<T extends object>(
    request: Request,
    response: JwtResponse<T>,
    next: NextFunction
) {
    const token = extractToken(request);
    if (token) {
        try {
            response.locals.payload = verifyToken<T>(token);
            response.locals.jwtToken = token;
            next();
        } catch (e) {
            response.status(401).send({ message: 'Invalid token', error: 'AUTHENTICATION_ERROR' });
        }
    } else if (response.locals.payload) {
        response.locals.jwtToken = createToken(response.locals.payload);
        next();
    } else {
        response.status(401).send({
            error: 'AUTHENTICATION_ERROR',
            message: 'No authentication token provided'
        });
    }
}

const createToken = <T extends object>(payload: T) =>
    sign(payload, 'seecret-value', { expiresIn: '2h' });
const verifyToken = <T extends object>(token: string) => verify(token, 'seecret-value') as T;

const extractToken = (request: Request) =>
    getAuthorizationHeader(request) ||
    request.params.token ||
    request.query.token ||
    (request.cookies || {}).token ||
    undefined;
const getAuthorizationHeader = (request: Request) =>
    (isArray(request.headers.authorization)
        ? request.headers.authorization
        : [request.headers.authorization || '']
    )
        .map(header => header.trim())
        .filter(header => header.match(/^Bearer\s.+/) != null)
        .map(authHeader => authHeader.split(' ')[1])[0];
