import * as bodyParser from 'body-parser';
import * as express from 'express';
import { check, validationResult } from 'express-validator/check';
import { matchedData } from 'express-validator/filter';
import { join } from 'path';
import * as request from 'request';

import { getSession } from './jiraApi';
import { jiraToken, JwtResponse } from './jwt';

export interface ApplicationConfig {
    readonly jiraUrl: string;
    readonly port: number;
    readonly publicResources: string;
}

interface TokenPayload {
    username: string;
    sessionData: string;
}

export function startJiraTools(options: ApplicationConfig) {
    const app = express();
    app.use(bodyParser.json());

    app.use((_req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header(
            'Access-Control-Allow-Headers',
            'Origin, X-Requested-With, Content-Type, authorization, Authorization, Accept'
        );
        next();
    });
    app.options('*', (req, res) => {
        res.header('Access-Control-Allow-Origin', req.headers.origin as string);
        res.header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
        res.header(
            'Access-Control-Allow-Headers',
            'Origin, X-Requested-With, Content-Type, authorization, Authorization, Accept'
        );
        res.header('Content-Length', '0');
        res.header('Access-Control-Allow-Credentials', 'true');
        res.status(204);
        res.end();
    });

    app.post(
        '/session',
        check('username')
            .trim()
            .exists()
            .withMessage('Jira username is required'),
        check('password')
            .trim()
            .exists()
            .withMessage('Jira password is required'),
        (req, res, next) => {
            const validationResults = validationResult(req);
            if (!validationResults.isEmpty()) {
                res.status(400).send(validationResults.mapped());
            } else {
                res.locals.jiraLoginData = matchedData(req);
                next();
            }
        },
        async (_req, res, next) => {
            const { username, password } = res.locals.jiraLoginData;
            try {
                const { session } = await getSession({
                    jiraUrl: options.jiraUrl,
                    password,
                    username
                });
                res.locals.payload = {
                    sessionData: `${session.name}=${session.value}`,
                    username
                };
                next();
            } catch (e) {
                res.status(401).send({ jiraAuthenticationError: true });
            }
        },
        jiraToken,
        (_req, response: JwtResponse<TokenPayload>) => {
            response.send({ token: response.locals.jwtToken });
        }
    );

    app.use(
        '/api',
        jiraToken,
        (req, res: JwtResponse<TokenPayload>, next) => {
            (req as any).tokenPayload = res.locals.payload;
            next();
        },
        (req, res) => {
            const sessionData = res.locals.payload.sessionData;
            const url = (path => {
                if (path.match(/^\/agile/)) {
                    return `/agile/1.0/${path.match(/^\/agile\/(.*)$/)![1]}`;
                } else {
                    return `/api/2${path}`;
                }
            })(req.path);

            const { authorization, referer, host, ...headers } = req.headers;
            request({
                headers: {
                    ...headers,
                    cookie: sessionData
                },
                method: req.method,
                qs: req.query,
                url: `${options.jiraUrl}/rest${url}`
            })
                .on('error', err => {
                    console.error('Error while proxying request to jira', err);
                })
                .pipe(res);
        }
    );
    app.use('/secure', jiraToken, (req, res) => {
        const sessionData = res.locals.payload.sessionData;
        const { authorization, referer, host, ...headers } = req.headers;
        request({
            headers: {
                ...headers,
                cookie: sessionData
            },
            method: req.method,
            qs: req.query,
            url: `${options.jiraUrl}/secure${req.path}`
        })
            .on('error', err => {
                console.error('Error while getting secured resources jira', err);
            })
            .pipe(res);
    });

    app.use(express.static(options.publicResources));
    app.get('*', (_req, res) => {
        res.sendFile(join(options.publicResources, 'index.html'));
    });
    app.listen(options.port, () => {
        console.log('server started on port:', options.port); // tslint:disable-line no-console
    });
}
