# TODO

* [ ] sprint board view
* [ ] double click to download issues dependencies (should hide other issues)
    * [ ] navigation, going back should navigate to previous set of issues
    * [ ] after double click try to download all dependencies, not only direct links (the whole tree)
* [ ] reset pan&zoom view to scale=1 and position to center
* [ ] reset force layout calculation
* [ ] print currently shown issues
* [ ] support links to/from epics (issues in epic)
* [ ] time in last status (from jira issue transitions)
