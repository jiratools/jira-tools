FROM node:8

WORKDIR /usr/src/app

COPY jira-proxy/package*.json ./
RUN npm install --only=production
COPY jira-proxy/dist/* ./
COPY jira-logging-app/dist/* ./public/
EXPOSE 80

CMD node index.js
