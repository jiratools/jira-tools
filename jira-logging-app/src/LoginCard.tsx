import classnames from 'classnames/bind';
import * as React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Dialog from 'react-toolbox/lib/dialog';
import Input from 'react-toolbox/lib/input';
import { ProgressBar } from 'react-toolbox/lib/progress_bar';

const cx = classnames.bind(require('./LoginCard.css')); // tslint:disable-line no-var-requires

export type LoginCardProps = {
    username?: string;
    password?: string;
    loading: boolean;
    open: boolean;
    loginChange: (login: string) => void;
    passwordChange: (password: string) => void;
    onSubmit: () => void;
};

const LOADING_TRANSITION_CLASS_NAMES = {
    enter: cx('loadingEnter'),
    enterActive: cx('loadingEnterActive'),
    leave: cx('loadingLeave'),
    leaveActive: cx('loadingLeaveActive')
};

export function LoginCard({
    username,
    password,
    onSubmit,
    loginChange,
    passwordChange,
    loading,
    open
}: LoginCardProps) {
    const dialogActions = [
        {
            accent: true,
            disabled: loading,
            label: 'Login',
            onClick: onSubmit,
            type: 'submit'
        }
    ];
    const onSubmitHandler = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        onSubmit();
    };
    return (
        <Dialog active={open} title="JiraLogin" actions={dialogActions}>
            <form onSubmit={onSubmitHandler}>
                <section style={{ position: 'relative' }}>
                    <Input
                        type="text"
                        label="Login"
                        name="login"
                        value={username}
                        onChange={loginChange}
                    />
                    <Input
                        type="password"
                        label="Password"
                        name="password"
                        value={password}
                        onChange={passwordChange}
                    />
                    <ReactCSSTransitionGroup
                        transitionEnterTimeout={200}
                        transitionLeaveTimeout={200}
                        transitionName={LOADING_TRANSITION_CLASS_NAMES}
                    >
                        {renderLoading(loading)}
                    </ReactCSSTransitionGroup>
                </section>
            </form>
        </Dialog>
    );
}

const renderLoading = (loading: boolean) =>
    loading && (
        <div className={cx('loadingWrapper')}>
            <ProgressBar type="circular" mode="indeterminate" multicolor={true} />
        </div>
    );
