export type Point = { x?: number; y?: number };
export type Line = { x1: number; y1: number; x2: number; y2: number };
export type Rect = { width: number; height: number };
export const ZERO_LINE: Readonly<Line> = { x1: 0, y1: 0, x2: 0, y2: 0 };
export const ZERO_RECT: Readonly<Rect> = { width: 0, height: 0 };

export function getCrossing(
    { x: x1 = 0, y: y1 = 0 }: Point,
    rectStart: Rect,
    { x: x2 = 0, y: y2 = 0 }: Point,
    rectEnd: Rect
): Line {
    const dx = x2 - x1;
    const dy = y2 - y1;
    if (dx === 0 && dy === 0) {
        return { x1, y1, x2, y2 };
    }
    const pStart =
        dx === 0
            ? rectStart.width / 2 / dy
            : dy === 0
            ? rectStart.height / 2 / dx
            : Math.min(
                  rectStart.width / Math.abs(dx),
                  rectStart.height / Math.abs(dy)
              ) / 2;
    const pEnd =
        dx === 0
            ? -rectEnd.width / 2 / dy
            : dy === 0
            ? -rectEnd.height / 2 / dx
            : Math.min(
                  rectEnd.width / Math.abs(dx),
                  rectEnd.height / Math.abs(dy)
              ) / 2;
    return {
        x1: x1 + pStart * dx,
        x2: x2 - pEnd * dx,
        y1: y1 + pStart * dy,
        y2: y2 - pEnd * dy
    };
}

export const getRectRadius = ({ height, width }: Rect) =>
    Math.sqrt(width * width + height * height) / 2;
export const getSizeChange = (rect1: Rect, rect2: Rect) =>
    Math.max(
        Math.abs(1 - rect1.width / rect2.width),
        Math.abs(1 - rect1.height / rect2.height)
    );

export const lineLength = (line: Line) =>
    Math.sqrt(Math.pow(line.x2 - line.x1, 2) + Math.pow(line.y2 - line.y1, 2));

export const getLinePart = (line: Line, start: number, end: number) => ({
    x1: line.x1 + start * (line.x2 - line.x1),
    x2: line.x1 + end * (line.x2 - line.x1),
    y1: line.y1 + start * (line.y2 - line.y1),
    y2: line.y1 + end * (line.y2 - line.y1)
});

export const isLeftToRightLine = (line: Line) => line.x1 < line.x2;
export const isLineUp = (line: Line) =>
    line.x1 === line.x2 && line.y1 >= line.y2;
export const reverseLine = (line: Line) => ({
    x1: line.x2,
    x2: line.x1,
    y1: line.y2,
    y2: line.y1
});
