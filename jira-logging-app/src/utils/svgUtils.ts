import { BaseType, Selection } from "d3";
import {
    getLinePart,
    isLeftToRightLine,
    isLineUp,
    Line,
    lineLength,
    reverseLine,
    ZERO_LINE
} from "./geometryUtils";

export function updateLine<T extends { line?: Line }>(
    line: Selection<SVGLineElement, T, BaseType, {}>,
    defaultLine: Line = ZERO_LINE
) {
    line.attr("x1", ({ line: { x1 } = defaultLine }) => x1)
        .attr("y1", ({ line: { y1 } = defaultLine }) => y1)
        .attr("x2", ({ line: { x2 } = defaultLine }) => x2)
        .attr("y2", ({ line: { y2 } = defaultLine }) => y2);
}

export const getTextPath = (
    text: string,
    linkLine: Line,
    padding: number,
    charWidth: number
): string => {
    const textLinePath = getLinePart(
        linkLine,
        padding,
        Math.min(
            (text.length * charWidth) / lineLength(linkLine) + padding,
            1 - padding
        )
    );
    return lineToPath(
        isLeftToRightLine(textLinePath) || isLineUp(textLinePath)
            ? textLinePath
            : reverseLine(textLinePath)
    );
};

export function createArrowMarker(
    selection: Selection<SVGSVGElement, {}, HTMLElement, {}>
) {
    const marker = selection
        .append("defs")
        .append("marker")
        .attr("markerWidth", "6")
        .attr("markerHeight", "10")
        .attr("refX", "6")
        .attr("refY", "5")
        .attr("orient", "auto")
        .attr("markerUnits", "strokeWidth");
    marker.append("path").attr("d", "M0,1 L1,0 L6,5 L1,10 L 0,9 L4,5 z");
    return marker;
}

export const lineToPath = (line: Line) =>
    `M ${line.x1},${line.y1} L ${line.x2},${line.y2}`;
