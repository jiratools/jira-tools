import classnames from "classnames/bind";
import { History } from "history";
import * as React from "react";
import { connect } from "react-redux";
import {
    NavLink,
    Redirect,
    Route,
    RouteComponentProps,
    Switch
} from "react-router-dom";
import { AppBar } from "react-toolbox/lib/app_bar";
import { ProgressBar } from "react-toolbox/lib/progress_bar";
import IssuesPrintout from "src/components/issuesPrintout/IssuesPrintout";
import {
    JiraIssuesProvider,
    ProviderType
} from "src/components/jiraIssuesProvider/Provider";

import { AppBarSwitch } from "./components/appBarSwitch/AppBarSwitch";
import { IssuesGraphApp } from "./IssuesGraphApp";
import { JiraIssue } from "./JiraApi";
import { Login } from "./Login";
import { State } from "./store";

const cx = classnames.bind(require("./App.css")); // tslint:disable-line no-var-requires

export type AppProps = {
    readonly loggedIn: boolean;
    readonly issuesState?: "LOADING" | "EMPTY";
} & RouteComponentProps<{}>;

export type AppState = {};

type ViewType = "graph" | "printout";

class AppComponent extends React.Component<AppProps, AppState> {
    constructor(props: AppProps) {
        super(props);

        this.state = {};
    }

    public render() {
        const toJql = { pathname: "/jql", search: this.props.location.search };
        const toBoards = {
            pathname: "/board",
            search: this.props.location.search
        };
        const viewType: ViewType =
            (new URLSearchParams(this.props.location.search).get(
                "view"
            ) as ViewType | null) || "printout";

        return (
            <div className={cx("appContainer")}>
                <AppBar title="React Toolbox" fixed={true}>
                    <div className={cx("navigation")}>
                        <AppBarSwitch
                            checked={viewType === "graph"}
                            onChange={this.switchView}
                            offLabel="Printout"
                            onLabel="Graph"
                        />
                        <NavLink to={toJql} activeClassName={cx("active")}>
                            JQL
                        </NavLink>
                        <NavLink to={toBoards} activeClassName={cx("active")}>
                            Agile board
                        </NavLink>
                    </div>
                </AppBar>
                <div style={{ marginTop: 80 }}>
                    <Switch>
                        <Route
                            path="/board"
                            render={this.renderBoardProvider}
                        />
                        <Route path="/jql" render={this.renderJqlProvider} />
                        <Redirect to="/jql" />
                    </Switch>
                </div>
                <div className={cx("appBody")}>
                    {this.renderApp(viewType)}
                    {this.renderState()}
                </div>
                <Login />
            </div>
        );
    }

    private switchView = (showGraph: boolean) => {
        const params = new URLSearchParams(this.props.location.search);
        params.set("view", showGraph ? "graph" : "printout");
        this.props.history.push({
            ...this.props.location,
            search: "?" + params
        });
    };

    private renderJqlProvider = ({
        location,
        history
    }: RouteComponentProps<{}>) =>
        this.renderIssuesProvider(ProviderType.JQL, location.search, history);
    private renderBoardProvider = ({
        location,
        history
    }: RouteComponentProps<{}>) =>
        this.renderIssuesProvider(ProviderType.BOARD, location.search, history);

    private renderIssuesProvider(
        type: ProviderType,
        query: string,
        history: History
    ) {
        return (
            this.props.loggedIn && (
                <JiraIssuesProvider
                    type={type}
                    query={query}
                    history={history}
                />
            )
        );
    }

    private renderState() {
        switch (this.props.issuesState) {
            case "LOADING":
                return (
                    <section className={cx("info-section")}>
                        <p>Loading issues...</p>
                        <ProgressBar mode="indeterminate" />
                    </section>
                );
            case "EMPTY":
                return (
                    <section className={cx("info-section")}>
                        <p>No issues</p>
                    </section>
                );
            default:
                return null;
        }
    }

    private renderApp(viewType: ViewType) {
        switch (viewType) {
            case "graph":
                return <IssuesGraphApp />;
            case "printout":
                return <IssuesPrintout />;
        }
    }
}

const getIssuesState = (state: State): "LOADING" | "EMPTY" | undefined =>
    state.issues.loading
        ? "LOADING"
        : state.issues.issues.length === 0
        ? "EMPTY"
        : undefined;

export const App = connect((state: State) => ({
    loggedIn: state.jiraAccess.token !== undefined,
    issuesState: getIssuesState(state)
}))(AppComponent);
