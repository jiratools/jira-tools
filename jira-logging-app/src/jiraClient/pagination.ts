interface AllPagesRequest<
    Result,
    Response,
    Params extends { startAt?: number }
> {
    fetchPage: (params: Params) => Promise<Response>;
    extractResult: (r: Response) => Result[];
    params: Pick<Params, Exclude<keyof Params, "startAt">>;
}

export async function fetchPages<
    Result,
    Response extends {
        readonly startAt?: number;
        readonly total?: number;
        readonly isLast?: boolean;
    },
    Params extends { readonly startAt?: number }
>({
    params,
    fetchPage,
    extractResult
}: AllPagesRequest<Result, Response, Params>): Promise<Result[]> {
    let response: Response;
    let startAt = 0;
    const result: Result[] = [];
    do {
        // tslint:disable-next-line:no-object-literal-type-assertion
        const request: Params = { ...params, startAt } as Params;
        response = await fetchPage(request);
        const data = extractResult(response);
        result.push(...data);
        startAt = (response.startAt || 0) + data.length;
    } while (response.isLast === false || startAt < (response.total || 0));
    return result;
}
