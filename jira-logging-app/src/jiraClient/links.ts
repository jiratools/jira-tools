import { JiraIssue, JiraIssueLink } from "../JiraApi";
import { fetchIssues } from "./issuesQurey";

/**
 *
 * @param token jira JWT token
 * @param maxLinkLength max distance from issues group to fetched issue (NaN for unlimited)
 */
export const collectLinks = (
    token: string,
    maxLinkLength: number = NaN
) => async (issues: JiraIssue[]) => {
    const collectedKeys = new Set(issues.map(getKey));
    const collectedIssues = [...issues];
    let newIssues = collectedIssues;
    let issuesToFetch: Set<string>;
    let linkLength = 1;

    do {
        if (!isNaN(maxLinkLength) && linkLength > maxLinkLength) {
            break;
        }
        issuesToFetch = new Set(
            newIssues
                .map(getLinkedIssuesKeys)
                .reduce(flatMap, [])
                .filter(filterOutCollected(collectedKeys))
        );
        if (issuesToFetch.size === 0) {
            return collectedIssues;
        }
        newIssues = await fetchIssues(token, ...issuesToFetch);
        linkLength++;
        collectedIssues.push(...newIssues);
        newIssues.map(getKey).forEach(addToSet(collectedKeys));
    } while (issuesToFetch.size);

    return collectedIssues;
};

const getKey = (issue: JiraIssue) => issue.key;
const getLinkedIssuesKeys = (issue: JiraIssue) =>
    issue.fields.issuelinks.map(getLinkedIssueKey);
const getLinkedIssueKey = (link: JiraIssueLink) => {
    if ("inwardIssue" in link) {
        return link.inwardIssue.key;
    } else {
        return link.outwardIssue.key;
    }
};

const addToSet = <T>(set: Set<T>) => (v: T) => set.add(v);

const filterOutCollected = (collected: Set<string>) => (key: string) =>
    !collected.has(key);

function flatMap<T>(acc: T[], current: T[]) {
    acc.push(...current);
    return acc;
}
