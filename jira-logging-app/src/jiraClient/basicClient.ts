import { stringify } from "query-string";

import { checkResponse, getFetchHeaders } from "./ajaxClient";

export interface JiraRequest {
    /**
     * jira authentication JWT token
     */
    readonly token: string;
    /**
     * jira rest api url
     */
    readonly baseUrl: string;
    readonly startAt?: number;
    /**
     * parameters that will be send to jira api
     */
    readonly [p: string]: string | number | boolean | undefined | null;
}

/**
 * Executes single call to jira rest api
 * @param request jira request params
 */
export function jiraRequest<T>({
    baseUrl,
    token,
    ...rest
}: JiraRequest): Promise<T> {
    return fetch(
        `${baseUrl}?${stringify({
            ...rest
        })}`,
        getFetchHeaders(token)
    )
        .then(checkResponse)
        .then(res => res.json());
}
