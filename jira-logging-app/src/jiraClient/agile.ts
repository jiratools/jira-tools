import {
    JiraBoard,
    JiraBoardsQueryResult,
    JiraIssue,
    JiraIssuesQueryResult,
    JiraSprint,
    JiraSprintsQueryResult,
    JiraSprintState
} from "./../JiraApi";
import { JiraRequest, jiraRequest } from "./basicClient";
import { fetchPages } from "./pagination";

export interface BoardsQuery {
    readonly token: string;
    readonly name?: string;
}
export interface SprintsQuery {
    readonly boardId: number;
    readonly token: string;
    readonly state?: JiraSprintState;
}
export interface IssuesOnBoardQuery {
    readonly board: JiraBoard;
    readonly sprint?: JiraSprint;
    readonly jql?: string;
    readonly token: string;
}

const catchError = <Args extends any[], R>(action: (...args: Args) => R) => (
    ...args: Args
) => {
    try {
        return action(...args);
    } catch (e) {
        if (e.error) {
            throw e.error;
        }
        throw e;
    }
};

export const fetchBoards = catchError((query: BoardsQuery) =>
    jiraRequest<JiraBoardsQueryResult>({
        baseUrl: `/api/agile/board`,
        token: query.token,
        name: query.name
    }).then(resp => resp.values)
);

export const fetchSprints = catchError((query: SprintsQuery) =>
    fetchPages<JiraSprint, JiraSprintsQueryResult, JiraRequest>({
        fetchPage: jiraRequest,
        extractResult: sprintsResult => sprintsResult.values,
        params: {
            baseUrl: `/api/agile/board/${query.boardId}/sprint`,
            token: query.token,
            state: query.state || ""
        }
    })
);

export async function fetchIssuesOnBoard({
    token,
    board,
    sprint,
    jql
}: IssuesOnBoardQuery): Promise<JiraIssue[]> {
    return fetchPages<JiraIssue, JiraIssuesQueryResult, JiraRequest>({
        fetchPage: jiraRequest,
        extractResult: issuesResult => issuesResult.issues,
        params: {
            baseUrl: `/api/agile/${
                board.type === "scrum" && sprint
                    ? `sprint/${sprint.id}`
                    : `board/${board.id}`
            }/issue`,
            fields: "*all",
            ...(jql ? { jql } : {}),
            token
        }
    });
}
