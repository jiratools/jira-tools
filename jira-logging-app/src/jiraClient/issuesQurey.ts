import { JiraIssue, JiraIssuesQueryResult } from "./../JiraApi";
import { JiraRequest, jiraRequest } from "./basicClient";
import { fetchPages } from "./pagination";

export function searchForIssues(
    token: string,
    jql: string
): Promise<JiraIssue[]> {
    return fetchPages<JiraIssue, JiraIssuesQueryResult, JiraRequest>({
        fetchPage: jiraRequest,
        extractResult: issuesResult => issuesResult.issues,
        params: {
            baseUrl: "/api/search",
            fields: "*all",
            jql,
            token
        }
    });
}

export const fetchIssues = (token: string, ...issuesKey: string[]) =>
    searchForIssues(token, keyQuery(issuesKey));

const keyQuery = (keys: string[]) => `key IN (${keys.join(", ")})`;
