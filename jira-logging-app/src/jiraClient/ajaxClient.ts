/**
 * Creates headers object for json authorized requests
 * @param token JWT barer token
 */
export function getFetchHeaders(token: string) {
    return {
        headers: {
            "Content-Type": "application/json",
            authorization: `Bearer ${token}`
        }
    };
}

/**
 * Check if response code is 200. Returns res for successfully calls, throws res for not 200 codes
 * @param res ajax response
 */
export function checkResponse(res: Response) {
    if (res.status !== 200) {
        throw res;
    }
    return res;
}
