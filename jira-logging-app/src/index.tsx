import "./index.css";

import React from "react";
import ReactDom from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route } from "react-router-dom";

import { App } from "./App";
import { configureStore, restoreInitialState } from "./store";

const store = configureStore(restoreInitialState());
const render = () =>
    ReactDom.render(
        <Provider store={store}>
            <Router>
                <Route path="/" component={App} />
            </Router>
        </Provider>,
        document.getElementById("app")
    );

render();

if ((module as any).hot) {
    (module as any).hot.accept("./App", render);
}
