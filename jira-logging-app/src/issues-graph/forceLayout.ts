import {
    forceCollide,
    ForceLink,
    forceLink,
    forceManyBody,
    forceSimulation,
    forceX,
    forceY,
    Simulation,
    SimulationLinkDatum,
    SimulationNodeDatum
} from "d3-force";

type ForceData<
    N extends SimulationNodeDatum,
    L extends SimulationLinkDatum<N>
> = {
    nodes: N[];
    links: L[];
    linkDistance: (link: L) => number;
    radius: (node: N) => number;
};
type ForceResult<
    N extends SimulationNodeDatum,
    L extends SimulationLinkDatum<N>
> = {
    layout: Simulation<N, L>;
    recalculateRadius: () => void;
    linksForce: ForceLink<N, L>;
    center(strong: boolean): void;
};
const CENTER_FORCE = 0.0015;
const CENTER_FORCE_STRONG = 0.1;
export function createForceLayout<
    N extends SimulationNodeDatum,
    L extends SimulationLinkDatum<N>
>({ nodes, links, linkDistance, radius }: ForceData<N, L>): ForceResult<N, L> {
    const collideForce = forceCollide(radius).strength(0.9);
    let recalculateRadiusTimeout: number;
    const linksForce = forceLink<N, L>(links).distance(linkDistance);
    const centerForce = {
        x: forceX(0).strength(CENTER_FORCE),
        y: forceY(0).strength(CENTER_FORCE)
    };
    return {
        layout: forceSimulation(nodes)
            .force("charge", forceManyBody().strength(-10))
            .force("links", linksForce)
            .force("x", centerForce.x)
            .force("y", centerForce.y)
            .force("collide", collideForce),
        linksForce,
        recalculateRadius: () => {
            if (!window) {
                collideForce.radius(radius);
                return;
            }
            window.clearTimeout(recalculateRadiusTimeout);
            recalculateRadiusTimeout = window.setTimeout(
                () => collideForce.radius(radius),
                5
            );
        },
        center(strong: boolean) {
            centerForce.x.strength(strong ? CENTER_FORCE_STRONG : CENTER_FORCE);
            centerForce.y.strength(strong ? CENTER_FORCE_STRONG : CENTER_FORCE);
        }
    };
}
