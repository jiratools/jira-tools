import { Point } from "./../utils/geometryUtils";
import { indexBy } from "ramda";

import { JiraIssue, JiraIssueLink, JiraIssueLinkType } from "./../JiraApi";
import { Link } from "./links";
import { Node } from "./nodes";

type NodesMap = { [key: string]: Node };

export function createNodes(
    issues: ReadonlyArray<JiraIssue>,
    prevNodes: Node[] = []
): Node[] {
    const issuesMap = indexBy(issueKey, issues);
    const nodes = prevNodes
        .filter(containsNode(issuesMap))
        .map(updateNode(issuesMap));
    const nodesMap = indexBy(nodeIssueKey, nodes);
    nodes.push(...issues.filter(notContainsIssue(nodesMap)).map(toNode));

    return nodes;
}

const issueKey = ({ key }: JiraIssue) => key;
const nodeIssueKey = ({ issue: { key } }: Node) => key;
const notContainsNode = (issuesMap: { [key: string]: JiraIssue }) => ({
    issue: { key }
}: Node) => issuesMap[key] === undefined;
const notContainsIssue = (nodesMap: { [key: string]: Node }) => ({
    key
}: JiraIssue) => nodesMap[key] === undefined;
const containsNode = (issuesMap: { [key: string]: JiraIssue }) => ({
    issue: { key }
}: Node) => issuesMap[key] !== undefined;
const containsIssue = (nodesMap: { [key: string]: Node }) => ({
    key
}: JiraIssue) => nodesMap[key] !== undefined;
const updateNode = (issuesMap: { [key: string]: JiraIssue }) => (
    node: Node
): Node => {
    node.issue = issuesMap[node.issue.key];
    return node;
};
const toNode = (issue: JiraIssue): Node => {
    const point: Point = {};
    const node = { issue, fx: undefined, fy: undefined };
    Object.defineProperty(node, "x", {
        get() {
            return point.x;
        },
        set(x: number) {
            point.x = x;
        }
    });
    Object.defineProperty(node, "y", {
        get() {
            return point.y;
        },
        set(y: number) {
            point.y = y;
        }
    });

    return node;
};

export function createLinks(nodes: Node[], withEpics: boolean = true): Link[] {
    return nodes
        .map(getIssueLinks(indexBy(node => node.issue.key, nodes), withEpics))
        .reduce((issueLinks, acc) => [...issueLinks, ...acc], [])
        .filter(filterOutDuplicates);
}

function filterOutDuplicates(
    link: Link & { type: JiraIssueLinkType },
    i: number,
    links: Link[]
) {
    const reverseLink = findReverseLink(link, links);
    if (reverseLink) {
        switch (link.type.name) {
            case "Blocks":
                return link.sourceName === link.type.outward;
            case "Cloners":
                return link.sourceName === link.type.outward;
            case "Relates":
                return i < links.indexOf(reverseLink);
            case "Child-Issue":
                return link.sourceName === link.type.inward;
            case "Family":
                return link.sourceName === link.type.inward;
            default:
                // tslint:disable-next-line:no-console
                console.warn("Unidentified links conflict", link, reverseLink);
                return link.type.inward === link.type.outward
                    ? i < links.indexOf(reverseLink)
                    : link.sourceName === link.type.outward;
        }
    }
    return true;
}

function findReverseLink(link: Link, links: Link[]): Link | undefined {
    return links.find(
        l2 => l2.source === link.target && l2.target === link.source
    );
}
const getIssueLinks = (nodesMap: NodesMap, withEpics: boolean) => (
    node: Node
) => {
    const links = node.issue.fields.issuelinks
        .map(convertToLink(nodesMap, node))
        .filter(link => link.target);
    if (
        withEpics &&
        node.issue.fields.customfield_10006 &&
        nodesMap[node.issue.fields.customfield_10006]
    ) {
        return [
            ...links,
            {
                source: node,
                sourceName: "Epic",
                target: nodesMap[node.issue.fields.customfield_10006],
                targetName: "Issue in epic",
                type: {
                    id: "0",
                    name: "Epic",
                    inward: "Epic",
                    outward: "Issue in epic",
                    self: "404 - not a real link in jira"
                }
            }
        ];
    }
    return links;
};

const convertToLink = (nodesMap: NodesMap, issue: Node) => (
    link: JiraIssueLink
) => {
    if ("inwardIssue" in link) {
        return {
            source: issue,
            sourceName: link.type.inward,
            target: nodesMap[link.inwardIssue.key],
            targetName: link.type.outward,
            type: link.type
        };
    } else {
        return {
            source: issue,
            sourceName: link.type.outward,
            target: nodesMap[link.outwardIssue.key],
            targetName: link.type.inward,
            type: link.type
        };
    }
};
