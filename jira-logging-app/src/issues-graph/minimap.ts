import classnames from "classnames/bind";
import { drag, event, select, Selection } from "d3";
import { ZoomBehavior, zoomTransform } from "d3-zoom";
import { ZERO_RECT } from "src/utils/geometryUtils";

import { Point, Rect } from "./../utils/geometryUtils";
import { GraphBoundaries } from "./nodes";
import { Node } from "./nodes/nodesFactory";

const cx = classnames.bind(require("./minimap.css")); // tslint:disable-line no-var-requires

type Minimap = Selection<SVGSVGElement, {}, any, any>;
type MinimapViewPort = Selection<SVGRectElement, {}, SVGSVGElement, {}>;
type MinimapView = {
    readonly minimap: Minimap;
    readonly viewPort: MinimapViewPort;
};
type MinimapData = {
    readonly graphMaxSize: number;
    readonly graphPosition: Required<Point>;
    readonly viewPortSize: Rect;
    readonly viewPortPosition: Required<Point>;
};

export function createMinimap(
    minimapWrapper: HTMLElement,
    getGraphBoundaries: () => GraphBoundaries,
    zoomRoot: HTMLElement,
    zoomBehavior: ZoomBehavior<HTMLElement, {}>
) {
    const minimap = createMinimapRoot(minimapWrapper);
    const view: MinimapView = {
        minimap,
        viewPort: createViewPort(minimap)
    };

    handleMinimapDragging(view.viewPort, zoomRoot, zoomBehavior, () =>
        calculateMinimapData(getGraphBoundaries, zoomRoot)
    );

    return {
        updateMinimap: updateMinimap(
            () => calculateMinimapData(getGraphBoundaries, zoomRoot),
            view
        )
    };
}

const createMinimapRoot = (minimapWrapper: Element): Minimap =>
    select(minimapWrapper)
        .append("svg")
        .classed(cx("minimap"), true)
        .attr("preserveAspectRatio", "xMidYMid meet");

const createViewPort = (
    minimap: Selection<SVGSVGElement, {}, any, any>
): MinimapViewPort =>
    minimap.append("rect").classed(cx("minimap-viewport"), true);

function handleMinimapDragging(
    viewPort: MinimapViewPort,
    zoomRoot: HTMLElement,
    zoomBehavior: ZoomBehavior<HTMLElement, {}>,
    calculateData: () => MinimapData
) {
    let data: MinimapData = calculateData();
    viewPort.call(
        drag<SVGRectElement, {}>()
            .on("start", () => (data = calculateData()))
            .on("drag", () =>
                zoomBehavior.translateTo(
                    select(zoomRoot),
                    event.x - data.graphPosition.x,
                    event.y - data.graphPosition.y
                )
            )
    );
}

const updateMinimap = (
    calculateData: () => MinimapData,
    { minimap, viewPort }: MinimapView
) => (nodes: Node[]) => {
    const { graphMaxSize, viewPortSize, viewPortPosition } = calculateData();
    minimap.attr("viewBox", `0 0 ${graphMaxSize} ${graphMaxSize}`);
    viewPort
        .attr("x", viewPortPosition.x)
        .attr("y", viewPortPosition.y)
        .attr("width", viewPortSize.width)
        .attr("height", viewPortSize.height);
    const nodeElement = minimap
        .selectAll("rect.node")
        .data(nodes)
        .attr("width", nodeWidth)
        .attr("height", nodeHeight)
        .attr("x", nodePositionX(graphMaxSize))
        .attr("y", nodePositionY(graphMaxSize));

    nodeElement
        .enter()
        .append("rect")
        .classed("node", true)
        .classed(cx("node"), true)
        .attr("width", nodeWidth)
        .attr("height", nodeHeight)
        .attr("x", nodePositionX(graphMaxSize))
        .attr("y", nodePositionY(graphMaxSize));

    nodeElement.exit().remove();
};

const nodeWidth = ({ size = ZERO_RECT }: Node) => size.width;
const nodeHeight = ({ size = ZERO_RECT }: Node) => size.height;
const nodePositionX = (graphMaxSize: number) => ({
    x = 0,
    size: { width } = ZERO_RECT
}: Node) => graphMaxSize / 2 + x - width / 2;
const nodePositionY = (graphMaxSize: number) => ({
    y = 0,
    size: { height } = ZERO_RECT
}: Node) => graphMaxSize / 2 + y - height / 2;

function calculateMinimapData(
    getGraphBoundaries: () => GraphBoundaries,
    zoomRoot: HTMLElement
): MinimapData {
    const boundaries = getGraphBoundaries();
    const zoom = zoomTransform(zoomRoot);
    const rootSize = zoomRoot.getBoundingClientRect();
    const graphSize = {
        width:
            Math.max(Math.abs(boundaries.xMax), Math.abs(boundaries.xMin)) * 2 +
            40,
        height:
            Math.max(Math.abs(boundaries.yMax), Math.abs(boundaries.yMin)) * 2 +
            40
    };
    const viewPortSize = {
        width: rootSize.width / zoom.k,
        height: rootSize.height / zoom.k
    };
    const graphMaxSize = Math.max(
        graphSize.width,
        graphSize.height,
        viewPortSize.width,
        viewPortSize.height
    );
    const graphPosition = {
        x: (graphMaxSize - graphSize.width) / 2,
        y: (graphMaxSize - graphSize.height) / 2
    };
    const viewPortPosition = {
        x: -zoom.x / zoom.k + graphPosition.x,
        y: -zoom.y / zoom.k + graphPosition.y
    };
    return {
        graphMaxSize,
        viewPortSize,
        viewPortPosition,
        graphPosition
    };
}
