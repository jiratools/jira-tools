export { default as Link } from "./Link";
export { getPreferredLinkLength, createLinks } from "./linksFactory";
