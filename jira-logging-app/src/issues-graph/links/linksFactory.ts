import classnames from "classnames/bind";
import { Selection } from "d3";
import { createArrowMarker, getTextPath } from "src/utils/svgUtils";

import { ZERO_LINE } from "../../utils/geometryUtils";
import { Line } from "./../../utils/geometryUtils";
import Link, { setupLinkLine } from "./Link";

const cx = classnames.bind(require("./links.css")); // tslint:disable-line no-var-requires

export function createLinks(selection: Selection<SVGSVGElement, {}, any, any>) {
    createArrowMarker(selection)
        .attr("id", "arrow")
        .classed(cx("graphLinkMarker"), true);

    function updateLinks(links: Link[]) {
        links.forEach(setupLinkLine);
        const linkElement = selection
            .selectAll<SVGGElement, Line>("g.line")
            .data(links);
        update(linkElement);
        enter(linkElement);
        exit(linkElement);
    }

    return { updateLinks };
}

function update(linkElement: Selection<SVGGElement, Link, SVGSVGElement, {}>) {
    linkElement
        .select("line")
        .attr("x1", setupX1)
        .attr("y1", setupY1)
        .attr("x2", setupX2)
        .attr("y2", setupY2);
    linkElement
        .select("path")
        .attr("id", linkId("link"))
        .attr("d", textPath(0.1, 9.64));
    linkElement
        .select("text")
        .select("textPath")
        .attr("href", linkId("#link"))
        .text(linkText);
}
function enter(linkElement: Selection<SVGGElement, Link, SVGSVGElement, {}>) {
    const newLinkElement = linkElement
        .enter()
        .append("g")
        .classed("line", true)
        .classed(cx("link"), true);
    newLinkElement
        .append("line")
        .attr("marker-end", "url(#arrow)")
        .attr("x1", setupX1)
        .attr("y1", setupY1)
        .attr("x2", setupX2)
        .attr("y2", setupY2);
    newLinkElement
        .append("path")
        .attr("id", linkId("link"))
        .attr("d", textPath(0.1, 9.64));
    newLinkElement
        .append("text")
        .append("textPath")
        .attr("href", linkId("#link"))
        .text(linkText);
}

function exit(linkElement: Selection<SVGGElement, Link, SVGSVGElement, {}>) {
    linkElement.exit().remove();
}

const setupX1 = ({ line: { x1 } = ZERO_LINE }: Link) => x1;
const setupY1 = ({ line: { y1 } = ZERO_LINE }: Link) => y1;
const setupX2 = ({ line: { x2 } = ZERO_LINE }: Link) => x2;
const setupY2 = ({ line: { y2 } = ZERO_LINE }: Link) => y2;

const linkId = (prefix: string) => ({ source, target }: Link) =>
    `${prefix}-${source.issue.key}-${target.issue.key}`;
const textPath = (padding: number, charWidth: number) => ({
    sourceName,
    line = ZERO_LINE
}: Link) => getTextPath(sourceName, line, padding, charWidth);
const linkText = ({ sourceName }: Link) => sourceName;

export function getPreferredLinkLength(link: Link) {
    return Math.max(200, link.sourceName.length * 9.64) + 200;
}
