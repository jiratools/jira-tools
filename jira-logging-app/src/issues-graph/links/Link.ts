import { getCrossing, Line } from "../../utils/geometryUtils";
import { Node } from "../nodes";

export default interface Link {
    readonly source: Node;
    readonly target: Node;
    readonly sourceName: string;
    readonly targetName: string;
    line?: Line;
}

/**
 * Calculates link.line based on source and target position and size.
 * @param link element to update
 */
export function setupLinkLine(link: Link) {
    link.line = getCrossing(
        getStart(link),
        getSourceSize(link),
        getEnd(link),
        getTargetSize(link)
    );
}

const getStart = (link: Link) => ({
    x: link.source.x || 0,
    y: link.source.y || 0
});
const getSourceSize = (link: Link) =>
    link.source.size || { width: 0, height: 0 };
const getTargetSize = (link: Link) =>
    link.target.size || { width: 0, height: 0 };
const getEnd = (link: Link) => ({
    x: link.target.x || 0,
    y: link.target.y || 0
});
