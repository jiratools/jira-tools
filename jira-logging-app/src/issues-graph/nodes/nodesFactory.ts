import classnames from "classnames/bind";
import { EnterElement, Selection, SimulationNodeDatum } from "d3";

import { JiraIssue, JiraStatusColor } from "../../JiraApi";
import { Rect } from "../../utils/geometryUtils";
import { ZERO_RECT } from "./../../utils/geometryUtils";

const cx = classnames.bind(require("./node.css")); // tslint:disable-line no-var-requires

export type Node = SimulationNodeDatum & {
    issue: JiraIssue;
    htmlNode?: Element;
    size?: Rect;
};

const STATUS_BACKGROUND: { [key in JiraStatusColor]: string } = {
    "blue-gray": "#4a6785",
    brown: "#815b3a",
    green: "#14892c",
    "medium-gray": "#ccc",
    "warm-red": "#d04437",
    yellow: "#ffd351"
};

const STATUS_COLOR: { [key in JiraStatusColor]: string } = {
    "blue-gray": "#fff",
    brown: "#fff",
    green: "#fff",
    "medium-gray": "#333",
    "warm-red": "#fff",
    yellow: "#594300"
};
export function createNodes<R extends HTMLElement>(
    selection: Selection<R, {}, any, any>
) {
    return {
        getGraphBoundaries,
        selectNodes() {
            return selection.selectAll<HTMLDivElement, Node>("div.node");
        },
        updateNodes(nodes: Node[]) {
            const nodeElement = selection
                .selectAll<HTMLDivElement, Node>("div.node")
                .data(nodes);
            update(nodeElement.merge(enter(nodeElement.enter())));
            exit(nodeElement.exit());
        },
        measureNodes(currentScale: number) {
            const measure = measureElement(currentScale);
            selection.selectAll<HTMLDivElement, Node>("div.node").each(measure);
            return measure.hasChanged();
        }
    };
}

function update(nodeElement: Selection<HTMLDivElement, Node, any, any>) {
    nodeElement.classed(cx("epic"), isEpic);
    nodeElement.select("h5 img").attr("src", getIcon);
    nodeElement.select("h5 span").text(getKey);
    nodeElement
        .select("h5 div")
        .style("background-color", getStatusColor(STATUS_BACKGROUND))
        .style("color", getStatusColor(STATUS_COLOR))
        .text(getStatus);
    nodeElement.select("div p").text(getSummary);
    nodeElement.style("transform", getTransformation);
}
function enter(nodeElement: Selection<EnterElement, Node, any, any>) {
    const newNodeElements = nodeElement
        .append("div")
        .classed("node", true)
        .classed(cx("card"), true);
    const newNodeHeader = newNodeElements
        .append("h5")
        .classed(cx("cardTitle"), true);
    newNodeHeader.append("img");
    newNodeHeader.append("span");
    newNodeHeader.append("div").classed(cx("status"), true);
    newNodeElements
        .append("div")
        .classed(cx("cardText"), true)
        .append("p");
    return newNodeElements;
}

function exit(nodeElement: Selection<HTMLDivElement, Node, any, any>) {
    nodeElement.remove();
}

const measureElement = (currentScale: number) => {
    let changed = false;
    function measureFunction(this: Element, node: Node) {
        const size = getSize(this, currentScale);
        const prevSize = node.size || ZERO_RECT;
        if (
            !compareNumbers(prevSize.width, size.width, 0.001) ||
            !compareNumbers(prevSize.height, size.height, 0.001)
        ) {
            changed = true;
            node.size = size;
        }
    }
    measureFunction.hasChanged = () => changed;
    return measureFunction;
};

function compareNumbers(a: number, b: number, precision: number) {
    return Math.abs(a - b) < Math.abs(precision);
}

const getSize = (element: Element, scale: number): Rect => {
    const { width, height } = element.getBoundingClientRect();
    return {
        width: width / scale,
        height: height / scale
    };
};

const getTransformation = (node: Node) =>
    `translate(${(node.x || 0) -
        (node.size || ZERO_RECT).width / 2}px, ${(node.y || 0) -
        (node.size || ZERO_RECT).height / 2}px)`;

const isEpic = (node: Node): boolean =>
    node.issue.fields.issuetype.name === "Epic";
const getIcon = (node: Node): string => node.issue.fields.issuetype.iconUrl;
const getKey = (node: Node): string => node.issue.key;
const getStatus = (node: Node): string => node.issue.fields.status.name;
const getSummary = (node: Node): string => node.issue.fields.summary;
const getStatusColor = (colorMap: { [key in JiraStatusColor]: string }) => (
    node: Node
) => colorMap[node.issue.fields.status.statusCategory.colorName];

export interface GraphBoundaries {
    readonly xMax: number;
    readonly xMin: number;
    readonly yMax: number;
    readonly yMin: number;
}
function getGraphBoundaries(nodes: Node[]): GraphBoundaries {
    return {
        xMax: nodes
            .map(({ x = 0, size: { width } = ZERO_RECT }) => x + width / 2)
            .reduce((x1, x2) => Math.max(x1, x2), 0),
        xMin: nodes
            .map(({ x = 0, size: { width } = ZERO_RECT }) => x - width / 2)
            .reduce((x1, x2) => Math.min(x1, x2), 0),
        yMax: nodes
            .map(({ y = 0, size: { height } = ZERO_RECT }) => y + height / 2)
            .reduce((y1, y2) => Math.max(y1, y2), 0),
        yMin: nodes
            .map(({ y = 0, size: { height } = ZERO_RECT }) => y - height / 2)
            .reduce((y1, y2) => Math.min(y1, y2), 0)
    };
}
