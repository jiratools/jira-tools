import { getRectRadius, ZERO_RECT } from "../../utils/geometryUtils";
import { Node } from "./nodesFactory";

export { createNodes, Node, GraphBoundaries } from "./nodesFactory";
export const getNodeRadius = (node: Node) =>
    getRectRadius(node.size || ZERO_RECT);
