import { event, select, Selection } from "d3";
import { Simulation, SimulationNodeDatum } from "d3-force";
import { zoom, ZoomBehavior, zoomTransform } from "d3-zoom";

import { Rect } from "./../utils/geometryUtils";

export function d3Zoom(graphNode: Selection<HTMLDivElement, {}, any, any>) {
    graphNode.style("transform-origin", "0 0");
    const zoomListeners: Array<() => void> = [];
    const zoomBehavior = zoom<HTMLElement, {}>().on("zoom", function(
        this: HTMLElement
    ) {
        zoomListeners.forEach(listener => listener());
    });
    return {
        adjustExtends: (root: HTMLElement, graphSize: Rect) =>
            adjustZoomExtents(root, graphSize, zoomBehavior),
        onZoom: (listener: () => void): void => {
            zoomListeners.push(listener);
        },
        zoom: zoomBehavior
    };
}
function adjustZoomExtents(
    root: HTMLElement,
    graphSize: Rect,
    zoomBehavior: ZoomBehavior<HTMLElement, any>
) {
    const rootSize = root.getBoundingClientRect();
    const rootSelection = select(root);
    const minZoom = Math.min(
        1,
        rootSize.width / graphSize.width,
        rootSize.height / graphSize.height
    );
    zoomBehavior
        .extent([[0, 0], [rootSize.width, rootSize.height]])
        .translateExtent([[0, 0], [graphSize.width, graphSize.height]])
        .scaleExtent([minZoom, 1.5]);
    zoomBehavior.translateBy(rootSelection, 0, 0);
    const currentZoom = zoomTransform(root).k;
    if (currentZoom < minZoom) {
        zoomBehavior.scaleBy(rootSelection, minZoom);
    }
}

export function d3Drag<N extends SimulationNodeDatum>(
    layout: Simulation<N, any>,
    getCurrentScale: () => number
) {
    const dragStartPosition = { x: 0, y: 0 };
    let currentScale = 1;
    return {
        dragEnd(node: N) {
            if (!event.active) {
                layout.alphaTarget(0);
            }
            node.fx = undefined;
            node.fy = undefined;
        },
        dragStart(node: N) {
            if (!event.active) {
                layout.alphaTarget(0.3).restart();
            }
            dragStartPosition.x = node.x || 0;
            dragStartPosition.y = node.y || 0;
            node.fx = node.x;
            node.fy = node.y;
            currentScale = getCurrentScale();
        },
        dragged(node: N) {
            node.fx =
                (event.x - dragStartPosition.x) / currentScale +
                dragStartPosition.x;
            node.fy =
                (event.y - dragStartPosition.y) / currentScale +
                dragStartPosition.y;
        }
    };
}
