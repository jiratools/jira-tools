import classnames from "classnames/bind";
import { select, Selection } from "d3";
import { drag } from "d3-drag";
import { zoomTransform } from "d3-zoom";

import { JiraIssue } from "../JiraApi";
import { d3Drag, d3Zoom } from "./d3Behaviors";
import { createForceLayout } from "./forceLayout";
import {
    createLinks as createLinksData,
    createNodes as createNodesData
} from "./graphDataMapper";
import { createLinks, getPreferredLinkLength, Link } from "./links";
import { createMinimap } from "./minimap";
import { createNodes, getNodeRadius, GraphBoundaries, Node } from "./nodes";

const cx = classnames.bind(require("./graph.css")); // tslint:disable-line no-var-requires

export interface Graph {
    cleanup(): void;
    setEpicLinks(withEpicLinks: boolean): void;
    setIssues(newIssues: ReadonlyArray<JiraIssue>): void;
    center(strong: boolean): void;
    zoomOut(): void;
}

export function createGraph(
    root: HTMLElement,
    issues: ReadonlyArray<JiraIssue>
): Graph {
    const rootSelection = select(root);
    let withEpicLinks = true;
    let nodes = createNodesData(issues);
    let links = createLinksData(nodes, withEpicLinks);
    const graphNode: Selection<
        HTMLDivElement,
        {},
        any,
        any
    > = rootSelection.append("div").classed(cx("graph"), true);
    const svg = graphNode.append("svg").classed(cx("links"), true);
    const { updateLinks } = createLinks(svg);
    const {
        selectNodes,
        updateNodes,
        getGraphBoundaries,
        measureNodes
    } = createNodes(graphNode.append("div").classed(cx("nodes"), true));
    updateNodes(nodes);
    measureNodes(1);

    const {
        layout,
        linksForce,
        recalculateRadius,
        ...force
    } = createForceLayout({
        linkDistance: getPreferredLinkLength,
        links,
        nodes,
        radius: getNodeRadius
    });
    const { zoom: zoomBehavior, onZoom, adjustExtends } = d3Zoom(graphNode);
    const currentScale = handleCurrentScale(root);
    onZoom(currentScale.update);
    onZoom(applyZoom(root, graphNode));
    const { dragEnd, dragStart, dragged } = d3Drag(layout, currentScale.get);
    rootSelection.call(zoomBehavior);
    selectNodes().call(
        drag<HTMLDivElement, Node>()
            .on("start", dragStart)
            .on("drag", dragged)
            .on("end", dragEnd)
    );
    const { updateMinimap } = createMinimap(
        root,
        () => getGraphBoundaries(nodes),
        root,
        zoomBehavior
    );

    const resizeHandler = () => {
        adjustExtends(root, {
            height: parseInt(graphNode.style("height"), 10),
            width: parseInt(graphNode.style("width"), 10)
        });
        updateMinimap(nodes);
    };
    window.addEventListener("resize", resizeHandler);

    layout.on("tick", () => {
        updateLinks(links);
        updateNodes(nodes);
        adjustGraphSize(graphNode, getGraphBoundaries(nodes));
        adjustExtends(root, {
            height: parseInt(graphNode.style("height"), 10),
            width: parseInt(graphNode.style("width"), 10)
        });
        updateMinimap(nodes);
    });
    onZoom(() => updateMinimap(nodes));
    return {
        cleanup() {
            layout.stop();
            graphNode.remove();
            selectNodes().remove();
            window.removeEventListener("resize", resizeHandler);
        },
        setEpicLinks(newWithEpicLinks: boolean) {
            if (newWithEpicLinks === withEpicLinks) {
                return;
            }
            withEpicLinks = newWithEpicLinks;
            links = createLinksData(nodes, newWithEpicLinks);
            linksForce.links(links);

            layout.alpha(1).restart();
        },
        setIssues(newIssues: JiraIssue[]) {
            const sameIssuesKeys = setEquals(
                new Set(nodes.map(getNodeKey)),
                new Set(newIssues.map(getIssueKey))
            );
            const prevLinksDefs = new Set(links.map(linkId));
            nodes = createNodesData(newIssues, nodes);
            links = createLinksData(nodes, withEpicLinks);
            const sameLinks = setEquals(
                prevLinksDefs,
                new Set(links.map(linkId))
            );
            updateNodes(nodes);
            const sizesChanged = measureNodes(currentScale.get());
            updateLinks(links);
            adjustGraphSize(graphNode, getGraphBoundaries(nodes));
            adjustExtends(root, {
                height: parseInt(graphNode.style("height"), 10),
                width: parseInt(graphNode.style("width"), 10)
            });
            updateMinimap(nodes);
            linksForce.links(links);
            layout.nodes(nodes);
            selectNodes().call(
                drag<HTMLDivElement, Node>()
                    .on("start", dragStart)
                    .on("drag", dragged)
                    .on("end", dragEnd)
            );
            if (sizesChanged || !sameIssuesKeys || !sameLinks) {
                recalculateRadius();
                layout.alpha(1);
                layout.restart();
            }
        },
        zoomOut() {
            zoomBehavior.scaleTo(rootSelection, 0);
        },
        center(strong: boolean) {
            force.center(strong);
            layout.alpha(1).restart();
        }
    };
}

function setEquals<T>(s1: Set<T>, s2: Set<T>) {
    if (s1.size !== s2.size) {
        return false;
    }
    for (const a of s1) {
        if (!s2.has(a)) {
            return false;
        }
    }
    return true;
}

const linkId = (link: Link) =>
    `${link.source.issue.key}-${link.sourceName}->${link.target.issue.key}`;
const getNodeKey = (node: Node) => node.issue.key;
const getIssueKey = (issue: JiraIssue) => issue.key;

function handleCurrentScale(element: Element) {
    let currentScale = 1;
    return {
        update: () => (currentScale = zoomTransform(element).k),
        get: () => currentScale
    };
}
const applyZoom = <T extends HTMLElement>(
    root: Element,
    element: Selection<T, any, any, any>
) => () => {
    const { x, y, k } = zoomTransform(root);
    element.style("transform", `translate(${x}px,${y}px) scale(${k})`);
};

function adjustGraphSize(
    graphNode: Selection<HTMLDivElement, {}, any, any>,
    graphBoundaries: GraphBoundaries
) {
    graphNode
        .style(
            "width",
            Math.max(
                Math.abs(graphBoundaries.xMax),
                Math.abs(graphBoundaries.xMin)
            ) *
                2 +
                40
        )
        .style(
            "height",
            Math.max(
                Math.abs(graphBoundaries.yMax),
                Math.abs(graphBoundaries.yMin)
            ) *
                2 +
                40
        );
    const svg = graphNode.select("svg");
    svg.style(
        "position",
        svg.style("position") === "absolute" ? "" : "absolute"
    );
}
