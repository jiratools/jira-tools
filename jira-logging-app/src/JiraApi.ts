interface JiraPaginationData {
    startAt: number;
    maxResults: number;
    total?: number;
    isLast: boolean;
}

export interface JiraIssuesQueryResult extends JiraPaginationData {
    expand: string;
    issues: JiraIssue[];
}

export interface JiraBoardsQueryResult extends JiraPaginationData {
    values: JiraBoard[];
}

export interface JiraSprintsQueryResult extends JiraPaginationData {
    values: JiraSprint[];
}

export interface JiraBoard {
    id: number;
    name: string;
    type: "kanban" | "scrum";
    slef: string;
}

export interface JiraSprint {
    id: number;
    self: string;
    state: JiraSprintState;
    name: string;
    startDate: string;
    endDate: string;
    completeDate?: string;
    originBoardId: number;
    goal?: string;
}

export type JiraSprintState = "closed" | "future" | "active";

export interface JiraIssue {
    expand: string;
    id: string;
    self: string;
    key: string;
    fields: {
        issuetype: JiraIssueType;
        /**
         * Time logged in seconds
         */
        timespent?: number | null;
        fixVersions: JiraVersion[];
        versions: JiraVersion[];
        project: JiraProject;
        /**
         * Agreggate time spent in seconds
         */
        aggregatetimespent?: number | null;
        resolution: JiraIssueResolution | null;
        resolutiondate: string | null;
        created: string;
        priority: JiraIssuePriority;
        labels: string[];
        issuelinks: JiraIssueLink[];
        assignee?: JiraUser | null;
        updated: string;
        status: JiraIssueStatus;
        components: JiraComponent[];
        timeestimate?: number | null;
        timeoriginalestimate?: number | null;
        aggregatetimeestimate?: number | null;
        description?: string | null;
        /**
         * Issue title
         */
        summary: string;
        creator: JiraUser;
        subtasks: JiraIssueSubtask[];
        reporter: JiraUser;
        aggregateprogress: JiraIssueProgress;
        progress: JiraIssueProgress;
        attachment?: JiraIssueAttachment[];
        comment?: {
            comments: JiraIssueComment[];
            maxResults: number;
            total: number;
            startAt: number;
        };
        worklog?: {
            startAt: number;
            maxResults: number;
            total: number;
            worklogs: JiraIssueWorklog[];
        };
        /**
         * Epic link - issue key
         */
        customfield_10006?: string;
        /**
         * Story points
         */
        customfield_10002?: number | null;
    };
}

export const JIRA_FIELD_EPIC = "customfield_10006";
export const JIRA_FIELD_STORY_POINTS = "customfield_10002";

export interface JiraProject {
    self: string;
    id: string;
    key: string;
    name: string;
    avatarUrls: JiraAvatars;
    projectCategory: {
        self: string;
        id: string;
        description: string;
        name: string;
    };
}

export type JiraAvatars = {
    [key in "16x16" | "24x24" | "32x32" | "48x48"]: string
};

export interface JiraIssueSubtask {
    id: string;
    key: string;
    self: string;
    fields: {
        summary: string;
        status: JiraIssueStatus;
        priority: JiraIssuePriority;
        issuetype: JiraIssueType;
    };
}

export interface JiraIssueType {
    self: string;
    id: string;
    description: string;
    iconUrl: string;
    name: string;
    subtask: boolean;
    avatarId?: number;
}

export interface JiraIssueResolution {
    self: string;
    id: string;
    description: string;
    name: string;
}

export interface JiraVersion {
    self: string;
    id: string;
    description: string;
    name: string;
    archived: boolean;
    released: boolean;
    releaseDate: string;
}

export interface JiraIssueProgress {
    progress: number;
    total: number;
    percent?: number;
}

export interface JiraComponent {
    self: string;
    id: string;
    name: string;
    description?: string;
}
export interface JiraIssueStatus {
    self: string;
    /**
     * Looks like it's usually empty
     */
    description: string;
    id: string;
    name: string;
    /**
     * Might be a generic image = useless
     */
    iconUrl: string;
    statusCategory: {
        self: string;
        id: number;
        key: JiraStatusKey;
        colorName: JiraStatusColor;
        name: string;
    };
}
export type JiraStatusColor =
    | "medium-gray"
    | "green"
    | "yellow"
    | "brown"
    | "warm-red"
    | "blue-gray";
export type JiraStatusKey = "new" | "indeterminate" | "done" | "undefined";

export interface JiraUser {
    self: string;
    name: string;
    key: string;
    emailAddress: string;
    displayName: string;
    active: boolean;
    timeZone: string;
    avatarUrls: JiraAvatars;
}

export interface JiraIssuePriority {
    self: string;
    iconUrl: string;
    name: string;
    id: string;
}

export type JiraIssueLink = JiraIssueInwardLink | JiraIssueOutwardLink;

export interface JiraIssueLinkType {
    id: string;
    /**
     * Link type name
     */
    name: string;
    /**
     * Relation name from parent: eg. This issue "is child of" issue in this link
     */
    inward: string;
    /**
     * Relation name from issue in link. eg Issue in link "is parent of" this issue
     */
    outward: string;
    self: string;
}
export interface JiraIssueInwardLink {
    id: string;
    self: string;
    type: JiraIssueLinkType;
    inwardIssue: {
        id: string;
        key: string;
        self: string;
        fields: {
            summary: string;
            status: JiraIssueStatus;
            priority: JiraIssuePriority;
            issuetype: JiraIssueType;
        };
    };
}
export interface JiraIssueOutwardLink {
    id: string;
    self: string;
    type: JiraIssueLinkType;
    outwardIssue: {
        id: string;
        key: string;
        self: string;
        fields: {
            summary: string;
            status: JiraIssueStatus;
            priority: JiraIssuePriority;
            issuetype: JiraIssueType;
        };
    };
}

export interface JiraIssueAttachment {
    self: string;
    id: string;
    filename: string;
    author: JiraUser;
    created: string;
    size: number;
    mimeType: string;
    content: string;
    thumbnail?: string;
}

export interface JiraIssueComment {
    self: string;
    id: string;
    author: JiraUser;
    body: string;
    updateAuthor?: JiraUser;
    created: string;
    updated?: string;
}

export interface JiraIssueWorklog {
    self: string;
    author: JiraUser;
    updateAuthor?: JiraUser;
    comment: string;
    created: string;
    updated?: string;
    started: string;
    timeSpent: string;
    timeSpentSeconds: number;
    id: string;
    issueId: string;
}
