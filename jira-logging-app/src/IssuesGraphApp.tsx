import classnames from "classnames/bind";
import * as React from "react";
import { ListCheckbox } from "react-toolbox/lib/list";
import { IconMenu } from "react-toolbox/lib/menu";
import { JiraIssue } from "src/JiraApi";
import { Button } from "react-toolbox/lib/button";

import { createGraph, Graph } from "./issues-graph/native-graph";
import { connect } from "react-redux";
import { State } from "./store";

const cx = classnames.bind(require("./IssuesGraphApp.css")); // tslint:disable-line no-var-requires

export type IssuesGraphAppProps = {
    className?: string;
    issues: ReadonlyArray<JiraIssue>;
};
export type IssuesGraphAppState = {
    withEpicLinks: boolean;
    settingsOpened: boolean;
    centered: boolean;
};
class IssuesGraphAppComponent extends React.Component<
    IssuesGraphAppProps,
    IssuesGraphAppState
> {
    private graphNode = React.createRef<HTMLDivElement>();
    private graph?: Graph;
    constructor(props: IssuesGraphAppProps) {
        super(props);
        this.state = {
            withEpicLinks: true,
            settingsOpened: false,
            centered: false
        };
    }

    public render() {
        return (
            <div className={cx(this.props.className, "graphWrapper")}>
                <div className={cx("graphRoot")} ref={this.graphNode} />
                <div className={cx("graphMenu")}>
                    <IconMenu
                        icon="more_vert"
                        position="topRight"
                        active={this.state.settingsOpened}
                        menuRipple={true}
                        onClick={this.openSettings}
                    >
                        <ListCheckbox
                            checked={this.state.withEpicLinks}
                            caption="Links to epic"
                            onChange={this.changeWithEpicLinks}
                        />
                    </IconMenu>
                </div>
                <div className={cx("centerButton")}>
                    <Button
                        icon="zoom_out_map"
                        title="Zoom out"
                        onClick={this.zoomOut}
                        floating={true}
                        accent={true}
                        mini={true}
                    />
                    <Button
                        icon={this.centerIcon}
                        title="Toggle strong center gravity"
                        onClick={this.center}
                        floating={true}
                        accent={true}
                        mini={true}
                    />
                </div>
            </div>
        );
    }

    public componentDidMount() {
        if (this.graphNode.current) {
            this.graph = createGraph(this.graphNode.current, this.props.issues);
        }
    }

    public componentDidUpdate(
        prevProps: IssuesGraphAppProps,
        prevState: IssuesGraphAppState
    ) {
        if (this.graph) {
            console.log("Updating graph");
            this.graph.setIssues(this.props.issues);
            if (prevState.centered !== this.state.centered) {
                this.graph.center(this.state.centered);
            }
        }
    }

    public componentWillUnmount() {
        if (this.graph) {
            this.graph.cleanup();
        }
        if (this.graphNode.current) {
            this.graphNode.current.innerHTML = "";
        }
    }

    private get centerIcon() {
        return this.state.centered
            ? "center_focus_strong"
            : "center_focus_weak";
    }
    private zoomOut = () => this.graph && this.graph.zoomOut();

    private center = () =>
        this.setState(({ centered }) => ({ centered: !centered }));

    private changeWithEpicLinks = (withEpicLinks: boolean) =>
        this.setState({ withEpicLinks }, () =>
            this.graph!.setEpicLinks(this.state.withEpicLinks)
        );
    private openSettings = () =>
        this.setState({
            settingsOpened: true
        });
}

export const IssuesGraphApp = connect((state: State) => ({
    issues: state.issues.issues
}))(IssuesGraphAppComponent);
