import classnames from "classnames/bind";
import { parse, stringify } from "query-string";
import * as React from "react";
import {
    JIRA_FIELD_STORY_POINTS,
    JiraIssue,
    JiraIssueAttachment,
    JiraIssueSubtask
} from "src/JiraApi";
import urlParse from "url-parse";
import { SubTaskCard } from "./SubTaskCard";
import { connect } from "react-redux";
import { State } from "src/store";

const cx = classnames.bind(require("./IssueCard.css")); // tslint:disable-line no-var-requires

interface IssueCardProps {
    issue: JiraIssue;
    epic?: JiraIssue;
    token: string;
}

function getAttachment(issue: JiraIssue): JiraIssueAttachment | undefined {
    if (!issue.fields.attachment || issue.fields.attachment.length <= 0) {
        return;
    }
    if (issue.key === "RETBCP-763") {
        const filtered = issue.fields.attachment.filter(
            element => element.filename === "WettAutomat_toRemove.png"
        );
        return filtered.length > 0 ? filtered[0] : undefined;
    } else {
        return issue.fields.attachment
            .reverse()
            .find(attachment => attachment.mimeType.startsWith("image"));
    }
}

function renderAttachment(
    withSubtasks: boolean,
    attachment: JiraIssueAttachment | undefined,
    token: string
) {
    if (!attachment) {
        return null;
    }
    const { pathname, query } = urlParse(attachment.content);
    const url = `${pathname}?${stringify({
        ...parse(query as any),
        token
    })}`;
    return (
        <div className={cx("image", withSubtasks && " with-subtasks")}>
            <img alt={attachment.filename} src={url} />
        </div>
    );
}

export const IssueCard = connect((state: State) => ({
    token: state.jiraAccess.token || ""
}))(({ issue, epic, token }: IssueCardProps) => {
    const cps202Link = epic && epic.key === "CPS-202";
    const isCrm = !epic && issue.fields.labels.indexOf("RetailCRM") >= 0;
    const storyPoints = issue.fields[JIRA_FIELD_STORY_POINTS];
    return (
        <div className={cx("issue-card")}>
            <div className={cx("top-bar")}>
                <span className={cx("key")}>{issue.key}</span>
                <span className={cx("title")}>{issue.fields.summary}</span>
            </div>
            <div className={cx("metadata")}>
                {cps202Link && <span className={cx("cps")}>CPS-202</span>}
                {epic && (
                    <span className={cx("epic")}>{epic.fields.summary}</span>
                )}
                {isCrm && <span className={cx("epic")}>Retail CRM</span>}
            </div>
            {renderAttachment(
                issue.fields.subtasks.length > 0,
                getAttachment(issue),
                token
            )}
            {!!storyPoints && (
                <div className={cx("estimate")}>{storyPoints}</div>
            )}
            {renderSubtasks(issue.fields.subtasks)}
        </div>
    );
});

const renderSubtasks = (subtasks: JiraIssueSubtask[]) =>
    subtasks.length > 0 && (
        <div className={cx("subtasks")}>
            {subtasks.map(subtask => (
                <SubTaskCard key={subtask.key} subtask={subtask} />
            ))}
        </div>
    );
