import classnames from 'classnames/bind';
import * as React from 'react';
import { JiraIssueSubtask } from 'src/JiraApi';

const cx = classnames.bind(require('./SubTaskCard.css')); // tslint:disable-line no-var-requires

interface SubTaskProps {
    subtask: JiraIssueSubtask;
}

export const SubTaskCard: React.SFC<SubTaskProps> = ({ subtask }) => (
    <div className={cx('subtask')}>
        <span className={cx('key')}>{subtask.key}</span>
        <span className={cx('title')}>{subtask.fields.summary}</span>
    </div>
);
