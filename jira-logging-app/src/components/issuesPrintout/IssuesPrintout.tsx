import indexBy from "ramda/src/indexBy";
import prop from "ramda/src/prop";
import * as React from "react";
import { connect } from "react-redux";
import { loginError, State } from "src/store";

import { JIRA_FIELD_EPIC, JiraIssue } from "../../JiraApi";
import { fetchIssues } from "../../jiraClient";
import { IssueCard } from "../issuesPrintout/IssueCard";

export interface IssuesPrintoutProps {
    issues: ReadonlyArray<JiraIssue>;
    jiraToken?: string;
    onError(message: string): void;
}

export interface IssuesPrintoutState {
    epicsMap: { [key: string]: JiraIssue };
}

export class IssuesPrintout extends React.Component<
    IssuesPrintoutProps,
    IssuesPrintoutState
> {
    constructor(props: IssuesPrintoutProps) {
        super(props);

        this.state = {
            epicsMap: {}
        };
    }

    public componentDidMount() {
        this.fetchEpics();
    }

    public componentDidUpdate() {
        this.fetchEpics();
    }

    public render() {
        return <div className="printout">{this.renterIssues()}</div>;
    }

    private renterIssues() {
        return this.props.issues.map(issue => (
            <IssueCard
                key={issue.key}
                issue={issue}
                epic={this.getEpic(issue)}
            />
        ));
    }

    private fetchEpics() {
        const epics = new Set<string>(this.props.issues
            .map(issue => issue.fields[JIRA_FIELD_EPIC])
            .filter(
                epicKey => epicKey && !this.state.epicsMap[epicKey]
            ) as string[]);
        if (epics.size > 0 && this.props.jiraToken) {
            fetchIssues(this.props.jiraToken, ...epics)
                .then(indexBy(prop("key")))
                .then(issues =>
                    this.setState(({ epicsMap }) => ({
                        epicsMap: {
                            ...epicsMap,
                            ...issues
                        }
                    }))
                )
                .catch(e => {
                    console.error("Unable to fetch epics for printout", e);
                    this.props.onError(
                        "Unable to fetch epics, try to re-login"
                    );
                });
        }
    }

    private getEpic(issue: JiraIssue): JiraIssue | undefined {
        const epicKey = issue.fields[JIRA_FIELD_EPIC];
        if (epicKey) {
            return this.state.epicsMap[epicKey];
        }
        return undefined;
    }
}

export default connect(
    (state: State) => ({
        issues: state.issues.issues,
        jiraToken: state.jiraAccess.token
    }),
    {
        onError: loginError
    }
)(IssuesPrintout);
