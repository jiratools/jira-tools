import classnames from "classnames/bind";
import * as React from "react";
import Button from "react-toolbox/lib/button";

import { JiraBoard, JiraSprint } from "../../JiraApi";
import { fetchBoards, fetchSprints } from "../../jiraClient";
import { Autocomplete, Option } from "../autocomplete/Autocomplete";

const cx = classnames.bind(require("./Selector.css")); // tslint:disable-line no-var-requires

export interface JiraBoardSelectorProps {
    jiraToken: string;
    onJiraRequestError: (request: Response) => void;
    onBoardSelected: (board: JiraBoard, sprint?: JiraSprint) => void;
    initialBoard?: string;
    initialSprint?: string;
}

export interface JiraBoardSelectorState {
    boards: JiraBoard[];
    boardsOptions: Option[];
    board?: Option;
    sprints: JiraSprint[];
    matchedSprints: Option[];
    sprint?: Option;
}

interface InitialData {
    board?: JiraBoard;
    sprints?: JiraSprint[];
}

export class JiraBoardSelector extends React.Component<
    JiraBoardSelectorProps,
    JiraBoardSelectorState
> {
    constructor(props: JiraBoardSelectorProps) {
        super(props);

        this.state = {
            boards: [],
            boardsOptions: [],
            matchedSprints: [],
            sprints: []
        };
    }

    public componentDidMount() {
        if (this.props.initialBoard) {
            fetchBoards({
                name: this.props.initialBoard,
                token: this.props.jiraToken
            })
                .then<InitialData>(boards => {
                    if (boards.length === 1) {
                        const [board] = boards;
                        if (
                            board.type === "scrum" &&
                            this.props.initialSprint
                        ) {
                            return fetchSprints({
                                boardId: board.id,
                                token: this.props.jiraToken
                            }).then(sprints => ({
                                board,
                                sprints
                            }));
                        }
                        return { board };
                    }
                    return {};
                })
                .then(({ board, sprints = [] }) => {
                    const sprintsOptions = sprints
                        .filter(
                            ({ name }) =>
                                this.props.initialSprint &&
                                this.props.initialSprint.match(name)
                        )
                        .map(sprintToOption);
                    this.setState({
                        board: board ? boardToOption(board) : undefined,
                        boards: board ? [board] : [],
                        boardsOptions: (board ? [board] : []).map(
                            boardToOption
                        ),
                        matchedSprints: sprintsOptions,
                        sprint: sprintsOptions[0],
                        sprints
                    });
                });
        }
    }

    public render() {
        return (
            <form className={cx("selector")} onSubmit={this.submit}>
                <div>
                    <Autocomplete
                        label="Select board"
                        options={this.state.boardsOptions}
                        value={this.state.board}
                        onQueryChange={this.searchBoards}
                        onChange={this.setBoardId}
                    />
                </div>
                <div>{this.getSprintInfo()}</div>
                <Button
                    icon="search"
                    label="Get issues"
                    raised={true}
                    primary={true}
                    disabled={!this.isBoardSelected()}
                    onClick={this.selectBoard}
                />
            </form>
        );
    }

    private isBoardSelected(): boolean {
        const board = this.state.board && this.getBoard(this.state.board);
        return !!(board && (board.type === "kanban" || this.state.sprint));
    }

    private getSprintInfo() {
        const board = this.state.board && this.getBoard(this.state.board);
        if (board && board.type === "scrum") {
            return (
                <Autocomplete
                    label="Select sprint"
                    options={this.state.matchedSprints}
                    value={this.state.sprint}
                    onQueryChange={this.filterSprints}
                    onChange={this.setSprint}
                />
            );
        }
        return null;
    }

    private getBoard(boardOption: Option): JiraBoard | undefined {
        return this.state.boards.find(board => board.id === boardOption.key);
    }

    private getSprint(sprintOption: Option): JiraSprint | undefined {
        return this.state.sprints.find(
            sprint => sprint.id === sprintOption.key
        );
    }

    private submit = (e: React.FormEvent) => {
        e.preventDefault();
        if (this.isBoardSelected()) {
            this.selectBoard();
        }
        return false;
    };

    private selectBoard = () => {
        const board = this.state.board && this.getBoard(this.state.board);
        if (board) {
            this.props.onBoardSelected(
                board,
                this.state.sprint
                    ? this.getSprint(this.state.sprint)
                    : undefined
            );
        }
    };

    private setBoardId = (boardOption: Option | undefined) => {
        this.setState({ board: boardOption });
        const board = boardOption && this.getBoard(boardOption);
        if (board && board.type === "scrum") {
            fetchSprints({
                boardId: board.id,
                token: this.props.jiraToken || ""
            })
                .then(sprints =>
                    this.setState({ sprints }, () => this.filterSprints(""))
                )
                .catch(e => {
                    if (e instanceof Response) {
                        this.props.onJiraRequestError(e);
                    } else {
                        throw e;
                    }
                });
        }
    };
    private setSprint = (sprint: Option | undefined) =>
        this.setState({ sprint });

    private filterSprints = (name: string) => {
        this.setState({
            matchedSprints: this.state.sprints
                .filter(sprint => name === "" || sprint.name.match(name))
                .map(sprintToOption)
        });
    };

    private searchBoards = (filter: string) =>
        fetchBoards({
            name: filter,
            token: this.props.jiraToken || ""
        })
            .then(boards => {
                this.setState({
                    boards,
                    boardsOptions: boards.map(boardToOption)
                });
            })
            .catch(e => {
                if (e instanceof Response) {
                    this.props.onJiraRequestError(e);
                } else {
                    throw e;
                }
            });
}

function boardToOption(board: JiraBoard): Option {
    return {
        key: board.id,
        name: board.name
    };
}

function sprintToOption(sprint: JiraSprint): Option {
    return {
        key: sprint.id,
        name: `${sprint.name} (${sprint.state})`
    };
}
