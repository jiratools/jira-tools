import { History } from "history";
import { parse, stringify } from "query-string";
import * as React from "react";
import { connect } from "react-redux";

import { JiraBoard, JiraSprint } from "../../JiraApi";
import { fetchBoards, fetchSprints } from "../../jiraClient";
import {
    fetchIssues as fetchIssuesAction,
    fetchSprint as fetchSprintAction,
    State,
    loginError
} from "../../store";
import { JiraBoardSelector } from "../jiraBoardSelector/Selector";
import JqlInput from "../jqlInput/JqlInput";
import { LinkLengthConfig } from "./LinkLengthConfig";

export enum ProviderType {
    BOARD,
    JQL
}
export interface JiraIssuesProviderProps {
    readonly jiraToken: string;
    readonly type: ProviderType;
    readonly query: string;
    readonly history: History;
    fetchByJql(jql: string, linkLength: number): void;
    fetchByBoard(
        board: JiraBoard,
        sprintName?: JiraSprint,
        linkLength?: number
    ): void;
    onJiraRequestError(request: Response): void;
}

export interface JiraIssuesProviderState {
    readonly numberOfLinks: number;
    readonly infiniteLinks: boolean;
    readonly noLinks: boolean;
}

type ParsedQuery = {
    readonly board?: string;
    readonly sprint?: string;
    readonly q?: string;
};

const filterSprints = (sprintName: string) => (sprints: JiraSprint[]) =>
    sprints.filter(({ name }) => sprintName && sprintName.match(name));
function fetchSprint(
    token: string,
    board?: JiraBoard,
    sprintName?: string
): Promise<JiraSprint | undefined> {
    if (board && sprintName && board.type === "scrum") {
        return fetchSprints({
            boardId: board.id,
            token
        })
            .then(filterSprints(sprintName))
            .then(([sprint]) => sprint);
    }
    return Promise.resolve(undefined);
}

function fetchBoardData(
    token: string,
    query: ParsedQuery
): Promise<{ board: JiraBoard; sprint?: JiraSprint } | undefined> {
    if (query.board) {
        return fetchBoards({
            name: query.board,
            token
        })
            .then(([board]) => board)
            .then(board =>
                fetchSprint(token, board, query.sprint).then(sprint => ({
                    board,
                    sprint
                }))
            );
    }
    return Promise.resolve(undefined);
}

class JiraIssuesProviderComponent extends React.Component<
    JiraIssuesProviderProps,
    JiraIssuesProviderState
> {
    private typeRenderers = {
        [ProviderType.JQL]: () => this.renderJqlSelector(),
        [ProviderType.BOARD]: () => this.renderBoardSelector()
    };
    private initialIssuesFetchers = {
        [ProviderType.JQL]: () => {
            const jql = parse(this.props.query).q as string;
            if (jql) {
                this.props.fetchByJql(jql, this.state.numberOfLinks);
            }
        },
        [ProviderType.BOARD]: () => {
            fetchBoardData(this.props.jiraToken, parse(this.props.query)).then(
                data => {
                    if (data) {
                        this.props.fetchByBoard(
                            data.board,
                            data.sprint,
                            this.state.numberOfLinks
                        );
                    }
                }
            );
        }
    };
    constructor(props: JiraIssuesProviderProps) {
        super(props);
        const numberOfLinks = Math.max(
            0,
            parseInt((parse(this.props.query).linksLength as string) || "0", 10)
        );
        this.state = {
            infiniteLinks: isNaN(numberOfLinks),
            numberOfLinks: numberOfLinks || 0,
            noLinks: numberOfLinks === 0
        };
    }

    public render() {
        return (
            <div>
                {(this.typeRenderers[this.props.type] || (() => null))()}
                <LinkLengthConfig setLinkLength={this.setMaxLinkLength} />
            </div>
        );
    }

    public componentDidMount() {
        this.initialIssuesFetchers[this.props.type]();
    }

    private setMaxLinkLength = (numberOfLinks: number) =>
        this.setState({ numberOfLinks: Math.max(0, numberOfLinks) });

    private renderBoardSelector() {
        const { board, sprint } = parse(this.props.query) as ParsedQuery;
        return (
            <JiraBoardSelector
                jiraToken={this.props.jiraToken}
                onJiraRequestError={this.props.onJiraRequestError}
                onBoardSelected={this.setBoard}
                initialBoard={board}
                initialSprint={sprint}
            />
        );
    }

    private renderJqlSelector() {
        return (
            <JqlInput
                onJqlEntered={this.setJql}
                initialJql={parse(this.props.query).q as string}
            />
        );
    }

    private setJql = (jql: string) => {
        this.props.history.push({
            ...this.props.history.location,
            search: stringify({
                ...parse(this.props.history.location.search),
                q: jql,
                linksLength: this.state.numberOfLinks
            })
        });
        this.props.fetchByJql(jql, this.state.numberOfLinks);
    };

    private setBoard = (board: JiraBoard, sprint?: JiraSprint) => {
        const { board: _1, sprint: _2, ...currentParams } = parse(
            this.props.history.location.search
        );
        this.props.history.push({
            ...this.props.history.location,
            search: stringify({
                ...currentParams,
                board: board.name,
                sprint: sprint ? sprint.name : undefined,
                linksLength: this.state.numberOfLinks
            })
        });
        this.props.fetchByBoard(board, sprint, this.state.numberOfLinks);
    };
}

export const JiraIssuesProvider = connect(
    (state: State) => ({
        jiraToken: state.jiraAccess.token || ""
    }),
    {
        fetchByJql: fetchIssuesAction,
        fetchByBoard: fetchSprintAction,
        onJiraRequestError: (resp: Response) => {
            console.error("Could not fetch issues", resp);
            loginError("Error while fetching issues, try to re-load");
        }
    }
)(JiraIssuesProviderComponent);
