import { parse } from "query-string";
import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router";
import FontIcon from "react-toolbox/lib/font_icon";
import Input from "react-toolbox/lib/input";
import RadioButton, { RadioGroup } from "react-toolbox/lib/radio";

// tslint:disable-next-line:no-var-requires
const styles = require("./LinkLengthConfig.css");

export type LinksRadioState = "no-links" | "number" | "infinity";
export const LINK_LENGTH_URL_KEY = "linksLength";

export interface LinkLengthConfigState {
    readonly numberOfLinks: number;
    readonly infiniteLinks: boolean;
    readonly noLinks: boolean;
}

export interface LinkLengthConfigProps extends RouteComponentProps<any> {
    setLinkLength(value: number): void;
}
export const LinkLengthConfig = withRouter(function LinkLengthConfigComponent(
    props: LinkLengthConfigProps
) {
    const [state, setState] = React.useState<LinkLengthConfigState>(
        getStateFromUrl(props)
    );
    const setLinkLength = React.useCallback(
        linkLengthSetter(state, setState, props.setLinkLength),
        [state, props.setLinkLength]
    );
    const setLinksRadioValue = React.useCallback(
        linkRadioSetter(state, setState, props.setLinkLength),
        [state, props.setLinkLength]
    );
    const linkRadioValue = getLinkConfigRadioState(state);

    const LinkLengthInput = (
        <Input
            disabled={linkRadioValue !== "number"}
            value={state.numberOfLinks || ""}
            hint="Max length of link"
            type="number"
            onChange={setLinkLength}
            theme={styles}
        />
    );
    return (
        <RadioGroup
            className={styles.radioGroup}
            name="links"
            value={linkRadioValue}
            onChange={setLinksRadioValue}
        >
            <RadioButton value="no-links" label="No additional issues" />
            <RadioButton value="number" label={LinkLengthInput} />
            <RadioButton
                value="infinity"
                label={<FontIcon value="all_inclusive" />}
            />
        </RadioGroup>
    );
});

const linkLengthSetter = (
    state: LinkLengthConfigState,
    setState: React.Dispatch<React.SetStateAction<LinkLengthConfigState>>,
    callback: LinkLengthConfigProps["setLinkLength"]
) => (linkLength: number) => {
    const newState = {
        ...state,
        numberOfLinks: Math.max(0, linkLength)
    };
    setState(newState);
    callback(getMaxLinkLength(newState));
};
const linkRadioSetter = (
    state: LinkLengthConfigState,
    setState: React.Dispatch<React.SetStateAction<LinkLengthConfigState>>,
    callback: LinkLengthConfigProps["setLinkLength"]
) => (value: LinksRadioState) => {
    const newState = calculateLinksState(state, value);
    setState(newState);
    callback(getMaxLinkLength(newState));
};

function getLinkConfigRadioState(
    state: LinkLengthConfigState
): LinksRadioState {
    if (state.infiniteLinks) {
        return "infinity";
    } else if (!state.noLinks) {
        return "number";
    }
    return "no-links";
}

function getMaxLinkLength(state: LinkLengthConfigState) {
    return state.noLinks ? 0 : state.infiniteLinks ? NaN : state.numberOfLinks;
}

function calculateLinksState(
    state: LinkLengthConfigState,
    value: LinksRadioState
): LinkLengthConfigState {
    switch (value) {
        case "infinity":
            return {
                infiniteLinks: true,
                noLinks: false,
                numberOfLinks: state.numberOfLinks
            };
        case "no-links":
            return {
                infiniteLinks: false,
                numberOfLinks: 0,
                noLinks: true
            };
        case "number":
            return {
                infiniteLinks: false,
                noLinks: false,
                numberOfLinks: state.numberOfLinks || 1
            };
    }
}

function getStateFromUrl(props: RouteComponentProps<any>) {
    const linkLength = Math.max(
        0,
        parseInt(
            (parse(props.location.search)[LINK_LENGTH_URL_KEY] as string) ||
                "0",
            10
        )
    );
    return {
        infiniteLinks: linkLength !== 0 && isNaN(linkLength || NaN),
        numberOfLinks: linkLength || 0,
        noLinks: linkLength === 0
    };
}
