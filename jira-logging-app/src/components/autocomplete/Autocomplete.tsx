import classnames from 'classnames/bind';
import * as React from 'react';
import { findDOMNode } from 'react-dom';
import Input, { InputProps } from 'react-toolbox/lib/input';

const cx = classnames.bind(require('./Autocomplete.css')); // tslint:disable-line no-var-requires

export interface Option {
    key: string | number;
    name: string;
}
export interface AutocompleteProps extends InputProps {
    label: string;
    options: Option[];
    onQueryChange: (query: string) => void;
    onChange: (item: Option) => void;
    value?: Option;
}

export interface AutocompleteState {
    currentSearch: string;
    focused: boolean;
    selection: number;
}

const KEY_ARROW_UP = 38;
const KEY_ARROW_DOWN = 40;
const KEY_ENTER = 13;

export class Autocomplete extends React.Component<AutocompleteProps, AutocompleteState> {
    private focused: HTMLElement | null = null;
    private list: HTMLElement | null = null;
    private input: React.Component<any> | null = null;
    constructor(props: AutocompleteProps) {
        super(props);

        this.state = {
            currentSearch: '',
            focused: false,
            selection: 0
        };
    }

    public render() {
        const { options, onQueryChange, onChange, value, ...props } = this.props;
        return (
            <div className={cx('autocomplete', this.state.focused && 'open')}>
                <Input
                    {...props}
                    value={this.getCurrentInputValue()}
                    onChange={this.changeSearch}
                    onBlur={this.onBlur}
                    onFocus={this.onFocus}
                    onKeyUp={this.onKeyUp}
                    ref={this.setInputElement}
                />
                <ul ref={this.setListElement}>{this.renderList(options)}</ul>
            </div>
        );
    }

    public componentDidUpdate() {
        if (this.focused && this.list) {
            const focusedRect = this.focused.getBoundingClientRect();
            const listRect = this.list.getBoundingClientRect();
            if (focusedRect.bottom > listRect.bottom) {
                this.list.scrollTop += focusedRect.bottom - listRect.bottom;
            } else if (focusedRect.top < listRect.top) {
                this.list.scrollTop -= listRect.top - focusedRect.top;
            }
        }
    }

    private renderList(options: Option[]) {
        return options.map((option, i) => (
            <li
                key={option.key}
                onMouseDown={this.setOption(option)}
                className={cx(i === this.state.selection && 'hover')}
                onMouseEnter={this.setSelection(i)}
                {...i === this.state.selection && { ref: this.setFocusedElement }}
            >
                {option.name}
            </li>
        ));
    }

    private getCurrentInputValue() {
        if (this.state.focused) {
            return this.state.currentSearch;
        } else if (this.props.value) {
            return this.props.value.name;
        }
        return '';
    }

    private onKeyUp = (e: KeyboardEvent) => {
        switch (e.which || e.keyCode) {
            case KEY_ENTER:
                this.setOption(this.props.options[this.state.selection])();
                if (this.input) {
                    const inputElement = findDOMNode(this.input) as Element;
                    (inputElement.querySelector('input') as HTMLElement).blur();
                }
                break;
            case KEY_ARROW_UP:
                this.setState({
                    selection: Math.max(this.state.selection - 1, 0)
                });
                break;
            case KEY_ARROW_DOWN:
                this.setState({
                    selection: Math.min(this.state.selection + 1, this.props.options.length - 1)
                });
                break;
        }
    };

    private setListElement = (ref: HTMLElement | null) => (this.list = ref);
    private setFocusedElement = (ref: HTMLElement | null) => (this.focused = ref);
    private setInputElement = (ref: React.Component<any> | null) => (this.input = ref);

    private setSelection = (selection: number) => () => {
        this.setState({ selection: selection % this.props.options.length });
    };

    private onBlur = (e: FocusEvent) => {
        if (this.state.focused) {
            this.setState({ focused: false });
        }
        if (this.props.onBlur) {
            this.props.onBlur(e);
        }
    };
    private onFocus = (e: FocusEvent) => {
        this.setState({ focused: true });
        if (this.props.onFocus) {
            this.props.onFocus(e);
        }
    };
    private changeSearch = (currentSearch: string) =>
        this.setState({ currentSearch }, () => this.props.onQueryChange(this.state.currentSearch));
    private setOption = (option: Option) => () => {
        this.props.onChange(option);
        this.setState({ currentSearch: option.name });
    };
}
