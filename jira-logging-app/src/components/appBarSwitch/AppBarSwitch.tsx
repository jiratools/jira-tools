import classnames from "classnames";

import * as React from "react";
import { Switch } from "react-toolbox/lib/switch";

const theme = require("./AppBarSwitch.css"); // tslint:disable-line no-var-requires

export interface AppBarSwitchProps {
    readonly checked: boolean;
    readonly onLabel: string;
    readonly offLabel: string;
    onChange(value: boolean): void;
}

export function AppBarSwitch(props: AppBarSwitchProps) {
    const on = () => props.onChange(true);
    const off = () => props.onChange(false);
    return (
        <>
            <label className={classnames(theme.labelOff)} onClick={off}>
                {props.offLabel}
            </label>
            <Switch
                checked={props.checked}
                onChange={props.onChange}
                theme={theme}
            />
            <label className={theme.labelOn} onClick={on}>
                {props.onLabel}
            </label>
        </>
    );
}
