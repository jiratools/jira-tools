import classnames from "classnames/bind";
import * as React from "react";
import { Button } from "react-toolbox/lib/button";
import { Input } from "react-toolbox/lib/input";

const cx = classnames.bind(require("./JqlInput.css")); // tslint:disable-line no-var-requires

export interface JqlInputProps {
    onJqlEntered: (jql: string) => void;
    initialJql?: string;
}

export interface JqlInputState {
    jql: string;
}

export default class JqlInput extends React.Component<
    JqlInputProps,
    JqlInputState
> {
    constructor(props: JqlInputProps) {
        super(props);

        this.state = {
            jql:
                this.props.initialJql || sessionStorage.getItem("lastJql") || ""
        };
    }

    public render() {
        return (
            <form className={cx("selector")} onSubmit={this.submit}>
                <Input
                    className={cx("searchInput")}
                    type="text"
                    label="JQL"
                    name="jql"
                    value={this.state.jql}
                    onChange={this.updateInput}
                />
                <Button
                    icon="search"
                    label="Get issues"
                    raised={true}
                    primary={true}
                    disabled={!this.state.jql}
                    onClick={this.setJql}
                />
            </form>
        );
    }

    private submit = (e: React.FormEvent) => {
        e.preventDefault();
        if (this.state.jql) {
            this.setJql();
        }
        return false;
    };
    private updateInput = (jql: string) => this.setState({ jql });
    private setJql = () => {
        this.props.onJqlEntered(this.state.jql);
        sessionStorage.setItem("lastJql", this.state.jql);
    };
}
