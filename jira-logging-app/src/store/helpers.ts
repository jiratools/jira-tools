import { ThunkAction } from "redux-thunk";
export interface Action<T extends string> {
    type: T;
}
export interface ActionWithPayload<T extends string, P> extends Action<T> {
    payload: P;
}

export function createAction<T extends string>(type: T): Action<T>;
export function createAction<T extends string, P>(
    type: T,
    payload: P
): ActionWithPayload<T, P>;
export function createAction<T extends string, P>(type: T, payload?: P) {
    return payload === undefined ? { type } : { type, payload };
}

type ActionCreator<T extends string> = (...args: any) => Action<T>;
type ThunkActionCreator = (...args: any) => ThunkAction<any, any, any, any>;
type ActionsCreators<T extends string> = {
    [creator: string]: ActionCreator<T> | ThunkActionCreator;
};
type SimpleActionsCreators<T extends string> = {
    [creator: string]: ActionCreator<T>;
};

export type ActionsUnion<
    T extends string,
    A extends ActionsCreators<T>
> = Values<ActionsMap<T, A>>;
type Values<T> = T[keyof T];

export type ActionsTypes<A extends ActionsCreators<any>> = ActionsUnion<
    any,
    A
>["type"];

type ActionsMap<
    Types extends string,
    Actions extends ActionsCreators<Types>
> = {
    [T in Types]: ReturnType<
        Extract<Values<Actions>, (...args: any) => Action<T>>
    >
};

type ActionsHandlers<
    State,
    Types extends string,
    Creators extends ActionsCreators<Types>
> = {
    [Type in keyof ActionsMap<Types, Creators>]: (
        state: State,
        action: ActionsMap<Types, Creators>[Type]
    ) => State
};

export function createReducer<
    State,
    Actions extends ActionsCreators<Types>,
    Types extends string
>(handlers: ActionsHandlers<State, Types, Actions>, initialState: State) {
    return (
        state: State = initialState,
        action: ActionsUnion<Types, Actions>
    ): State => {
        if (action.type in handlers) {
            return handlers[action.type](state, action);
        }
        return state;
    };
}
