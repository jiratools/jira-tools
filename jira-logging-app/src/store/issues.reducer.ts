import debug from "debug";
import { ThunkAction } from "redux-thunk";

import { State as RootState } from ".";
import { JiraIssue, JiraBoard, JiraSprint } from "../JiraApi";
import {
    collectLinks,
    searchForIssues,
    fetchIssuesOnBoard
} from "../jiraClient";
import { Action, ActionsTypes, createAction, createReducer } from "./helpers";
import { ACTIONS as ACCESS_ACTIONS } from "./jira-access.reducer";

const LOG = debug("store:reducer:issues");

export type State = Readonly<{
    issues: ReadonlyArray<JiraIssue>;
    loading: boolean;
}>;

export const SET_ISSUES = "@jiraIssues/set";
export const ADD_ISSUES = "@jiraIssues/add";
export const LOADING_ISSUES = "@jiraIssues/loading";

export const ACTIONS = {
    setIssues: (issues: ReadonlyArray<JiraIssue>) =>
        createAction(SET_ISSUES, issues),
    addIssues: (issues: ReadonlyArray<JiraIssue>) =>
        createAction(ADD_ISSUES, issues),
    clearIssues: () => createAction(SET_ISSUES, [] as ReadonlyArray<JiraIssue>),
    setLoading: () => createAction(LOADING_ISSUES),
    fetchIssues: (
        jql: string,
        linkLength: number = 0
    ): ThunkAction<void, RootState, any, Action<any>> => (
        dispatch,
        getState
    ) => {
        dispatch(ACTIONS.setLoading());
        const token = getState().jiraAccess.token;
        if (!token) {
            LOG("Cannot fetch issues without valid token", new Error());
            dispatch(ACTIONS.clearIssues());
            return;
        }
        searchForIssues(token, jql)
            .then(collectLinks(token, linkLength))
            .then(ACTIONS.setIssues)
            .then(dispatch)
            .catch(() => dispatch(ACCESS_ACTIONS.invalidateToken()));
    },
    fetchSprint: (
        board: JiraBoard,
        sprint?: JiraSprint,
        linkLength: number = 0
    ): ThunkAction<void, RootState, any, Action<any>> => (
        dispatch,
        getState
    ) => {
        dispatch(ACTIONS.setLoading());
        const token = getState().jiraAccess.token;
        if (!token) {
            LOG("Cannot fetch issues without valid token", new Error());
            dispatch(ACTIONS.clearIssues());
            return;
        }
        fetchIssuesOnBoard({
            board,
            sprint,
            token
        })
            .then(collectLinks(token, linkLength))
            .then(ACTIONS.setIssues)
            .then(dispatch)
            .catch(() => dispatch(ACCESS_ACTIONS.invalidateToken()));
    }
};

export const reducer = createReducer<
    State,
    typeof ACTIONS,
    ActionsTypes<typeof ACTIONS>
>(
    {
        [SET_ISSUES]: ({ issues, ...state }, { payload }) => ({
            ...state,
            issues: [...payload],
            loading: false
        }),
        [ADD_ISSUES]: ({ issues, ...state }, { payload }) => ({
            ...state,
            issues: [...issues, ...payload],
            loading: false
        }),
        [LOADING_ISSUES]: state => ({
            ...state,
            loading: true
        })
    },
    {
        issues: [],
        loading: false
    }
);
