import { fetchBoards } from "./../jiraClient/agile";
import { applyMiddleware, combineReducers, createStore, compose } from "redux";
import thunk from "redux-thunk";

import {
    ACTIONS as ISSUES_ACTIONS,
    reducer as issues,
    State as IssuesState
} from "./issues.reducer";
import {
    ACTIONS as ACCESS_ACTIONS,
    reducer as jiraAccess,
    State as AccessState
} from "./jira-access.reducer";

export function configureStore(initialState?: State) {
    const composeEnhancers: typeof compose =
        (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    return createStore(
        combineReducers({
            jiraAccess,
            issues
        }),
        initialState,
        composeEnhancers(applyMiddleware(thunk))
    );
}

export function restoreInitialState(): State {
    return {
        issues: {
            issues: [],
            loading: false
        },
        jiraAccess: {
            loading: false,
            token: sessionStorage.getItem("jiraToken") || undefined
        }
    };
}

export const { login: loginUser, loginError } = ACCESS_ACTIONS;
export const { fetchSprint, fetchIssues } = ISSUES_ACTIONS;
export type State = {
    readonly jiraAccess: AccessState;
    readonly issues: IssuesState;
};
