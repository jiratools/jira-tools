import debug from "debug";
import { ThunkAction } from "redux-thunk";

import { checkResponse } from "../jiraClient";
import { Action, ActionsTypes, createAction, createReducer } from "./helpers";

const LOG = debug("store:reducer:jira-access");
export type State = Readonly<{
    token?: string;
    loading: boolean;
    loginError?: string;
}>;

interface JiraLoginResponse {
    readonly token: string;
}
const jiraLoginFetchConfig = (
    username: string,
    password: string
): RequestInit => ({
    body: JSON.stringify({ username, password }),
    credentials: "omit",
    headers: new Headers({
        "Content-Type": "application/json"
    }),
    method: "POST"
});

export const SET_TOKEN_TYPE = "@jiraAuth/setToken";
export const SET_ERROR_TYPE = "@jiraAuth/setError";
export const INVALIDATE_TOKEN_TYPE = "@jiraAuth/invalidateToken";
export const START_LOGIN_TYPE = "@jiraAuth/startLogin";

export const ACTIONS = {
    setToken: (token: string) => createAction(SET_TOKEN_TYPE, token),
    invalidateToken: () => createAction(INVALIDATE_TOKEN_TYPE),
    startLogin: () => createAction(START_LOGIN_TYPE),
    loginError: (message: string) => createAction(SET_ERROR_TYPE, message),
    login: (
        username: string,
        password: string
    ): ThunkAction<() => void, undefined, any, Action<any>> => dispatch => {
        dispatch(ACTIONS.startLogin());
        const abortController = new AbortController();
        fetch("/session", {
            ...jiraLoginFetchConfig(username, password),
            signal: abortController.signal
        })
            .then(checkResponse)
            .then<JiraLoginResponse>(response => response.json())
            .then(({ token }) => dispatch(ACTIONS.setToken(token)))
            .catch(e => {
                LOG("Error while logging user", e);
                dispatch(ACTIONS.loginError("The error occurred"));
            });
        return () => abortController.abort();
    }
};

export const reducer = createReducer<
    State,
    typeof ACTIONS,
    ActionsTypes<typeof ACTIONS>
>(
    {
        [SET_TOKEN_TYPE]: ({ loginError: _, ...state }, action) => ({
            ...state,
            token: action.payload,
            loading: false
        }),
        [INVALIDATE_TOKEN_TYPE]: ({ token: _, ...state }) => state,
        [START_LOGIN_TYPE]: ({ loginError: _, loading, ...state }) => ({
            ...state,
            loading: true
        }),
        [SET_ERROR_TYPE]: ({ token, ...state }, action) => ({
            ...state,
            loading: false,
            loginError: action.payload
        })
    },
    {
        loading: false
    }
);
