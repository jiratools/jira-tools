import * as React from "react";
import { connect } from "react-redux";

import { LoginCard } from "./LoginCard";
import { loginUser, State } from "./store";

export type LoginProps = {
    open: boolean;
    loading: boolean;
    login(username: string, password: string): void;
};

export const Login = connect(
    (state: State) => ({
        open: state.jiraAccess.token === undefined,
        loading: state.jiraAccess.loading
    }),
    {
        login: (username: string, password: string) =>
            loginUser(username, password)
    }
)(function LoginContainer(props: LoginProps) {
    const [username, setUsernameState] = React.useState("");
    const [password, setPasswordState] = React.useState("");
    const login = React.useCallback(() => props.login(username, password), [
        username,
        password
    ]);
    const setUsername = React.useCallback(
        (v: string) => setUsernameState(v),
        []
    );
    const setPassword = React.useCallback(
        (v: string) => setPasswordState(v),
        []
    );

    return (
        <LoginCard
            username={username}
            password={password}
            loading={props.loading}
            open={props.open}
            onSubmit={login}
            loginChange={setUsername}
            passwordChange={setPassword}
        />
    );
});
