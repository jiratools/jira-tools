# Jira tools application

Web application that provides additional functionalities to jira:

- printing of issues in the nice format
- drawing graph of issues based on issues links
- ... more will come

It's required to host this application from the server that will handle authentication via JWT and proxies of requests to the jira server.
