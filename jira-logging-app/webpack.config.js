const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require("webpack");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
    .BundleAnalyzerPlugin;
const Dotenv = require("dotenv-webpack");
const path = require("path");
const postcssColorMod = require("postcss-color-mod-function");
module.exports = (env, data) => {
    const dev = data.mode === "development";
    return {
        entry: dev
            ? [
                  "react-hot-loader/patch",
                  "abortcontroller-polyfill/dist/polyfill-patch-fetch",
                  "./src"
              ]
            : ["abortcontroller-polyfill/dist/polyfill-patch-fetch", "./src"],
        resolve: {
            extensions: [".js", ".tsx", ".ts", ".json", ".css"],
            alias: {
                src: path.resolve("./src")
            }
        },
        output: dev
            ? {}
            : {
                  filename: "[name].[chunkhash].js"
              },
        devtool: "source-map",
        mode: data.mode,
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "ts-loader"
                    }
                },
                {
                    test: /\.css$/,
                    use: [
                        dev ? "style-loader" : MiniCssExtractPlugin.loader,
                        {
                            loader: "css-loader",
                            options: {
                                modules: true,
                                sourceMap: true,
                                importLoaders: 1,
                                localIdentName:
                                    "[name]--[local]--[hash:base64:8]"
                            }
                        },
                        {
                            loader: "postcss-loader",
                            options: {
                                ident: "postcss",
                                plugins: () => [
                                    require("postcss-import")(),
                                    require("postcss-preset-env")({
                                        stage: 0,
                                        features: {
                                            "color-mod-function": true // required to use color-mod()
                                        }
                                    }),
                                    require("postcss-color-mod-function")({
                                        transformVars: true
                                    }),
                                    require("autoprefixer")({ grig: true })
                                ]
                            }
                        }
                    ]
                }
            ]
        },
        devServer: {
            host: "0.0.0.0",
            historyApiFallback: true,
            hot: true,
            proxy: {
                "/api": "http://localhost:3000",
                "/session": "http://localhost:3000",
                "/secure": "http://localhost:3000"
            }
        },
        optimization: dev
            ? {}
            : {
                  runtimeChunk: "single",
                  splitChunks: {
                      cacheGroups: {
                          vendor: {
                              test: /[\\/]node_modules[\\/]/,
                              name: "vendors",
                              chunks: "all"
                          }
                      }
                  }
              },
        plugins: [
            new HtmlWebPackPlugin({
                template: "./src/index.html",
                filename: "./index.html"
            }),
            // new webpack.LoaderOptionsPlugin({
            //     debug: true
            // }),
            new Dotenv(),
            ...(dev
                ? [
                      new webpack.HotModuleReplacementPlugin(),
                      new webpack.NamedModulesPlugin()
                  ]
                : [
                      new CleanWebpackPlugin(["dist"]),
                      new webpack.HashedModuleIdsPlugin(),
                      new MiniCssExtractPlugin({
                          filename: "[name].[chunkhash].css",
                          chunkFilename: "[id].[chunkhash].css"
                      }),
                      new BundleAnalyzerPlugin({
                          analyzerMode: "static",
                          openAnalyzer: false,
                          reportFilename: "../reports/bundleSize.html"
                      })
                  ])
        ]
    };
};
